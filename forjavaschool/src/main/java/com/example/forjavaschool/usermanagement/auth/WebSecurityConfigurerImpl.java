package com.example.forjavaschool.usermanagement.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfigurerImpl extends WebSecurityConfigurerAdapter {

    /*private static final String adminUserName = "admin";
    private static final String adminPassword = "admin123";
    private static final String readOnlyUserName = "readonly";
    private static final String readOnlyPassword = "readonly123";*/
    private static final String leysanUserName = "leysan";
    private static final String leysanPassword = "leysan123";
    private static final String shopUser = "shop";
    private static final String shopUserPassword = "shop123";

    @Autowired
    UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService) // db users
                .passwordEncoder(getEncoder());
        auth
                .inMemoryAuthentication() // hardcoded users
                .withUser(shopUser)
                .password(getEncoder().encode(shopUserPassword))
                .roles("USER")
                .and()
                .withUser(leysanUserName)
                .password(getEncoder().encode(leysanPassword))
                .roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .authorizeRequests()
                //TODO: попроавить эндпоинты так, чтобы не зависело от порядка
                .mvcMatchers("/admin/device_images/files/**").permitAll()
                //admin pages (only tariffs/all available for non-admin)
                .mvcMatchers("/admin/tariffs/all").hasAnyRole("ADMIN", "USER")
                .mvcMatchers("/admin/**").hasRole("ADMIN") //и так доступно и юзеру


                //forShop endpoints, new_tariff SHOULD be available for admin, all - for user and admin:
                .mvcMatchers("/forshop/tariffs/new_tariff").hasRole("ADMIN") //работает без аутентификации
                .mvcMatchers("/forshop/**").hasAnyRole("ADMIN", "USER")





                .anyRequest().authenticated()
                 .and()
                .formLogin()
                .and()
                .httpBasic(); // (3)
    }

    @Bean
    public PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }
}
