package com.example.forjavaschool.usermanagement.dao;

import com.example.forjavaschool.usermanagement.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(unique = true)
    @Size(min = 3, max = 50)
    @NotBlank(message = "Username is mandatory")
    private String username;

    @Column
    @Size(min = 3)
    @NotBlank(message = "Password is mandatory")
    private String password;

    @Column
    private String role; // should be prefixed with ROLE_

    @Column
    private boolean enabled;

    public User() {
        this.enabled = true;
    }

    public User(UserDTO userDTO) {
        this();
        this.username = userDTO.getUsername();
      //  this.password = userDTO.getPassword();
        this.role = userDTO.getRole();
        this.enabled = userDTO.isEnabled();
    }

}
