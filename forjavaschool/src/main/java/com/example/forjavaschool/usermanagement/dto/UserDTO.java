package com.example.forjavaschool.usermanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserDTO {
    @Size(min = 3, max = 50)
    @NotBlank(message = "Username is mandatory")
    private String username;

    @Size(min = 3)
    @NotBlank(message = "Password is mandatory")
    private String password;

    private String role;// should be prefixed with ROLE_

    private boolean enabled;



}
