package com.example.forjavaschool.usermanagement.repositories;

import com.example.forjavaschool.usermanagement.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepo extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    boolean existsByUsername(String username);



}
