package com.example.forjavaschool.usermanagement.service;

import com.example.forjavaschool.usermanagement.dao.User;
import com.example.forjavaschool.usermanagement.dto.UserDTO;
import com.example.forjavaschool.usermanagement.repositories.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UsersRepo usersRepo;

    @Autowired
    PasswordEncoder encoder;

    public List<User> getAllUsers() {
        return usersRepo.findAll();
    }

    public User registerUser(UserDTO userDTO) {
        User user = new User(userDTO);
        user.setPassword(encoder.encode(userDTO.getPassword()));
        return usersRepo.save(user);
    }


}
