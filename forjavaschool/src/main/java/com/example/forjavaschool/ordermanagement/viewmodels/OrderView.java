package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.ordermanagement.dao.*;

public class OrderView {
    OrderConfigurationItemsView orderConfigurationItemsView;
    AddressView orderAddressView;
    PersonalDataView personalDataView;
    PassportDataView passportDataView;

    public OrderView() {
    }

    public OrderView(Order order) {
        OrderConfigurationItems configurationItems = order.getOrderConfigurationItems();
        this.orderConfigurationItemsView = new OrderConfigurationItemsView(configurationItems);
        Address address = order.getAddress();
        if (address != null) {
            this.orderAddressView = new AddressView(address);
        }
        PersonalData personalData = order.getPersonalData();
        if (personalData != null) {
            this.personalDataView = new PersonalDataView(personalData);
        }
        PassportData passportData = order.getPassportData();
        if (passportData != null) {
            this.passportDataView = new PassportDataView(passportData);
        }

    }

    public void setOrderConfigurationItemsView(OrderConfigurationItemsView orderConfigurationItemsView) {
        this.orderConfigurationItemsView = orderConfigurationItemsView;
    }

    public PersonalDataView getPersonalDataView() {
        return personalDataView;
    }

    public void setPersonalDataView(PersonalDataView personalDataView) {
        this.personalDataView = personalDataView;
    }

    public AddressView getOrderAddressView() {
        return orderAddressView;
    }

    public void setOrderAddressView(AddressView orderAddressView) {
        this.orderAddressView = orderAddressView;
    }

    public OrderConfigurationItemsView getOrderConfigurationItemsView() {
        return orderConfigurationItemsView;
    }
}
