package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.ordermanagement.dao.PassportData;
import com.example.forjavaschool.ordermanagement.dao.PassportType;

import java.util.Date;

public class PassportDataView {
    private String passportTypeName;
    private String passportNumber;
    private Date issueDate;

    public PassportDataView() {
    }

    public PassportDataView(PassportData passportData) {
        this.passportTypeName = passportData.getPassportType().name();
        this.passportNumber = passportData.getPassportNumber();
        this.issueDate = passportData.getIssueDate();
    }

    public PassportDataView(String passportTypeName, String passportNumber, Date issueDate) {
        this.passportTypeName = passportTypeName;
        this.passportNumber = passportNumber;
        this.issueDate = issueDate;
    }

    public String getPassportTypeName() {
        return passportTypeName;
    }

    public void setPassportTypeName(String passportTypeName) {
        this.passportTypeName = passportTypeName;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}
