package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.ordermanagement.dao.Address;

public class AddressView {
    private int postcode;
    private String city;
    private String street;
    private int houseNr;
    private String houseAddition;
    private int flatNr;

    public AddressView() {
    }

    public AddressView(int postcode, String city, String street, int houseNr, String houseAddition, int flatNr) {
        this.postcode = postcode;
        this.city = city;
        this.street = street;
        this.houseNr = houseNr;
        this.houseAddition = houseAddition;
        this.flatNr = flatNr;
    }

    public AddressView(Address address) {
        this.postcode = address.getPostcode();
        this.city = address.getCity();
        this.street = address.getStreet();
        this.houseNr = address.getHouseNr();
        this.houseAddition = address.getHouseAddition();
        this.flatNr = address.getFlatNr();
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(int houseNr) {
        this.houseNr = houseNr;
    }

    public String getHouseAddition() {
        return houseAddition;
    }

    public void setHouseAddition(String houseAddition) {
        this.houseAddition = houseAddition;
    }

    public int getFlatNr() {
        return flatNr;
    }

    public void setFlatNr(int flatNr) {
        this.flatNr = flatNr;
    }
}
