package com.example.forjavaschool.ordermanagement.dao;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;


import java.util.Set;

public class OrderConfigurationItems {
    private Offer offer;
    private Tariff tariff;
    private Device device;
    private Set<Option> options;


    public OrderConfigurationItems() {
    }


    public Offer getOffer() {
        return offer;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public Device getDevice() {
        return device;
    }

    public Set<Option> getOptions() {
        return options;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }
}
