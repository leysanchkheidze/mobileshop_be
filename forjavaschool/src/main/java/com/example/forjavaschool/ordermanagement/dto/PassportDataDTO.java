package com.example.forjavaschool.ordermanagement.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class PassportDataDTO {
    @NotNull
    int typeID;
    @NotBlank
    private String passportNumber;
    @NotNull
    private Date issueDate;

    public PassportDataDTO(int typeID, String passportNumber, Date issueDate) {
        this.typeID = typeID;
        this.passportNumber = passportNumber;
        this.issueDate = issueDate;
    }



    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}
