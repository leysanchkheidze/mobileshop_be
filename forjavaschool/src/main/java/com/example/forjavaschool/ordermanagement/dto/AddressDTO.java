package com.example.forjavaschool.ordermanagement.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddressDTO {
    @NotNull
    private int postcode;
    @NotBlank
    private String city;
    @NotBlank
    private String street;
    @NotNull
    private int houseNr;
    private String houseAddition;
    private int flatNr;


    public AddressDTO() {
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public AddressDTO(int postcode, String city, String street, int houseNr, String houseAddition, int flatNr) {
        this.postcode = postcode;
        this.city = city;
        this.street = street;
        this.houseNr = houseNr;
        this.houseAddition = houseAddition;
        this.flatNr = flatNr;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNr() {
        return houseNr;
    }

    public String getHouseAddition() {
        return houseAddition;
    }

    public int getFlatNr() {
        return flatNr;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouseNr(int houseNr) {
        this.houseNr = houseNr;
    }

    public void setHouseAddition(String houseAddition) {
        this.houseAddition = houseAddition;
    }

    public void setFlatNr(int flatNr) {
        this.flatNr = flatNr;
    }
}
