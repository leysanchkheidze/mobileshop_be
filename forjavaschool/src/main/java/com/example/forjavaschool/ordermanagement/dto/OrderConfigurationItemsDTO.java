package com.example.forjavaschool.ordermanagement.dto;

import java.util.Set;

public class OrderConfigurationItemsDTO {
    private int offerID;
    private int tariffID;
    private int deviceID;
    private Set<Integer> optionIDs;


    public int getOfferID() {
        return offerID;
    }

    public Set<Integer> getOptionIDs() {
        return optionIDs;
    }

    public int getTariffID() {
        return tariffID;
    }

    public int getDeviceID() {
        return deviceID;
    }
}
