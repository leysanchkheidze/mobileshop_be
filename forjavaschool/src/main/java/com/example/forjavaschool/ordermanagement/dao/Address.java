package com.example.forjavaschool.ordermanagement.dao;

import com.example.forjavaschool.ordermanagement.dto.AddressDTO;

public class Address {
    private int postcode;
    private String city;
    private String street;
    private int houseNr;
    private String houseAddition;
    private int flatNr;

    public Address() {
    }

    public Address(int postcode, String city, String street, int houseNr, String houseAddition, int flatNr) {
        this.postcode = postcode;
        this.city = city;
        this.street = street;
        this.houseNr = houseNr;
        this.houseAddition = houseAddition;
        this.flatNr = flatNr;
    }

    public Address(AddressDTO addressDTO) {
        this.postcode = addressDTO.getPostcode();
        this.city = addressDTO.getCity();
        this.street = addressDTO.getStreet();
        this.houseNr = addressDTO.getHouseNr();
        this.houseAddition = addressDTO.getHouseAddition();
        this.flatNr = addressDTO.getFlatNr();
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(int houseNr) {
        this.houseNr = houseNr;
    }

    public String getHouseAddition() {
        return houseAddition;
    }

    public void setHouseAddition(String houseAddition) {
        this.houseAddition = houseAddition;
    }

    public int getFlatNr() {
        return flatNr;
    }

    public void setFlatNr(int flatNr) {
        this.flatNr = flatNr;
    }
}


