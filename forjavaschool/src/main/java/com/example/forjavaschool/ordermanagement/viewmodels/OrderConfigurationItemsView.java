package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.ordermanagement.dao.OrderConfigurationItems;
import com.example.forjavaschool.catalogmanagement.viewmodels.DeviceInOfferView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OfferView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffInOfferView;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class OrderConfigurationItemsView {
    private OfferView offerView;
    private TariffInOfferView tariffInOfferView;
    private DeviceInOfferView deviceInOfferView;
    private SortedSet<OptionPlainView> optionViews;

    public OrderConfigurationItemsView() {
    }

    public OrderConfigurationItemsView(OrderConfigurationItems configurationItems) {
        Offer offer = configurationItems.getOffer();
        this.offerView = new OfferView(offer);
        Tariff tariff = configurationItems.getTariff();
        this.tariffInOfferView = new TariffInOfferView(tariff, offer);
        Device device = configurationItems.getDevice();
        this.deviceInOfferView = new DeviceInOfferView(device, offer);
        Set<Option> options = configurationItems.getOptions();
        Supplier<SortedSet<OptionPlainView>> opts = TreeSet::new;
        this.optionViews = options.stream()
                .map(OptionPlainView::new)
                .sorted()
                .collect(Collectors.toCollection(opts));
    }

    public OfferView getOfferView() {
        return offerView;
    }

    public TariffInOfferView getTariffInOfferView() {
        return tariffInOfferView;
    }

    public DeviceInOfferView getDeviceInOfferView() {
        return deviceInOfferView;
    }

    public SortedSet<OptionPlainView> getOptionViews() {
        return optionViews;
    }
}