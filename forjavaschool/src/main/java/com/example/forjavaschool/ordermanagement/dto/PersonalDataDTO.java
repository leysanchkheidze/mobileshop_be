package com.example.forjavaschool.ordermanagement.dto;

import com.example.forjavaschool.ordermanagement.dao.PersonalData;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class PersonalDataDTO {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotNull
    private Date birthday;
    @NotNull
    private PersonalData.Gender gender;

    public PersonalDataDTO() {
    }

    public PersonalDataDTO(String firstName, String lastName, Date birthday, PersonalData.Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public PersonalData.Gender getGender() {
        return gender;
    }

    public void setGender(PersonalData.Gender gender) {
        this.gender = gender;
    }
}
