package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.ordermanagement.dao.PassportType;

public class PassportTypeView {
    private PassportType type;
    private String displayName;
    private int typeID;

    //TODO: Боря, я наверное дебил, но что-то не пойму, как без этой штуки получить енум с его полями
    //rest api enum java
    //https://www.baeldung.com/spring-enum-request-param

    public PassportTypeView(PassportType type) {
        this.type = type;
        this.displayName = type.getDisplayName();
        this.typeID = type.getTypeID();
    }

    public PassportType getType() {
        return type;
    }

    public void setType(PassportType type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }
}
