package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.viewmodels.CompatibleOptionGroupView;
import com.example.forjavaschool.catalogmanagement.viewmodels.DeviceInOfferView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffInOfferView;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.SortedSet;

@Getter
@AllArgsConstructor
public class OrderConfigurationView {
    //offer data:
    private int offerId;

    //tariff data:
    private TariffInOfferView tariffInOfferView;

    //device data:
    private DeviceInOfferView deviceInOfferView;

    //compatible options in groups:
    private SortedSet<CompatibleOptionGroupView> compatibleOptionsInGroups;

    //compatible options without groups:
    private SortedSet<OptionPlainView> compatibleOptionsWithoutGroups;

}
