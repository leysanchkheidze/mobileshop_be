package com.example.forjavaschool.ordermanagement.dao;

import com.example.forjavaschool.ordermanagement.dto.PersonalDataDTO;

import java.util.Date;

public class PersonalData {
    private String firstName;
    private String lastName;
    private Date birthday;
    private Gender gender;

    public PersonalData() {
    }

    public PersonalData(PersonalDataDTO personalDataDTO) {
        this.firstName = personalDataDTO.getFirstName();
        this.lastName = personalDataDTO.getLastName();
        this.birthday = personalDataDTO.getBirthday();
        this.gender = personalDataDTO.getGender();
    }

    public PersonalData(String firstName, String lastName, Date birthday, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public enum Gender {
        F, M
    }
}
