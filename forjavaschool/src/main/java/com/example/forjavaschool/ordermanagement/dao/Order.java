package com.example.forjavaschool.ordermanagement.dao;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.SessionScope;

//TODO: где правильно хранить этот класс, это не энтити, в базе не лежит
@Configuration
@SessionScope
public class Order {
    private OrderConfigurationItems orderConfigurationItems;
    private PersonalData personalData;
    private Address address;
    private PassportData passportData;


    public OrderConfigurationItems getOrderConfigurationItems() {
        return orderConfigurationItems;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public Address getAddress() {
        return address;
    }

    public PassportData getPassportData() {
        return passportData;
    }

    public void setOrderConfigurationItems(OrderConfigurationItems orderConfigurationItems) {
        this.orderConfigurationItems = orderConfigurationItems;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPassportData(PassportData passportData) {
        this.passportData = passportData;
    }


}
