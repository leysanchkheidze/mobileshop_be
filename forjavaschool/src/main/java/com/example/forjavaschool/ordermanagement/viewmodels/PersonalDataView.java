package com.example.forjavaschool.ordermanagement.viewmodels;

import com.example.forjavaschool.ordermanagement.dao.PersonalData;

import java.util.Date;

public class PersonalDataView {
    private String firstName;
    private String lastName;
    private Date birthday;
    private PersonalData.Gender gender;

    public PersonalDataView() {
    }

    public PersonalDataView(PersonalData personalData) {
        this.firstName = personalData.getFirstName();
        this.lastName = personalData.getLastName();
        this.birthday = personalData.getBirthday();
        this.gender = personalData.getGender();
    }


    public PersonalDataView(String firstName, String lastName, Date birthday, PersonalData.Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public PersonalData.Gender getGender() {
        return gender;
    }

    public void setGender(PersonalData.Gender gender) {
        this.gender = gender;
    }
}
