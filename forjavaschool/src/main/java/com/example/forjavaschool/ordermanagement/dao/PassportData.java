package com.example.forjavaschool.ordermanagement.dao;

import com.example.forjavaschool.ordermanagement.dto.PassportDataDTO;

import java.util.Date;

public class PassportData {
    PassportType passportType;
    private String passportNumber;
    private Date issueDate;

    public PassportData() {
    }

    public PassportData(PassportType passportType, String passportNumber, Date issueDate) {
        this.passportType = passportType;
        this.passportNumber = passportNumber;
        this.issueDate = issueDate;
    }

    public PassportData(PassportDataDTO passportDataDTO) {
        this.passportType = PassportType.getPassportType(passportDataDTO.getTypeID());
        this.passportNumber = passportDataDTO.getPassportNumber();
        this.issueDate = passportDataDTO.getIssueDate();
    }

    public PassportType getPassportType() {
        return passportType;
    }

    public void setPassportType(PassportType passportType) {
        this.passportType = passportType;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}

