package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dto.OptionDTO;
import com.example.forjavaschool.catalogmanagement.service.EntityCannotBeSavedException;
import com.example.forjavaschool.catalogmanagement.service.OptionGroupsService;
import com.example.forjavaschool.catalogmanagement.service.OptionsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import com.example.forjavaschool.utils.BindingResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@PropertySource("messages.properties")
@RequestMapping("${endpoints.admin.options.controller_path}")
public class OptionsPageController {

    @Autowired
    OptionsService optionsService;

    @Autowired
    OptionGroupsService optionGroupsService;

    @Value("${endpoints.admin.options.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.options.new}")
    private String newOptionPath;
    @Value("${endpoints.admin.options.edit}")
    private String editOptionPath;
    @Value("${directory.templates.options}")
    private String templateFolder;
    @Value("${general.price.nonnumber.msg}")
    private String priceDigitsMessage;

    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newOptionPath);
        model.addAttribute("pathEdit", controllerPath + editOptionPath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{id}");
        model.addAttribute("controllerPath", controllerPath);
    }

    @GetMapping
    public String getOptionsPage(Model model) {
        model.addAttribute("options", optionsService.findAllOptionViews());
        return templateFolder + "options";
    }

    @GetMapping("${endpoints.admin.options.new}")
    public String showNewOptionPage(OptionDTO optionDTO, Model model) {
        model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());
        return templateFolder + "new_option";
    }

    @PostMapping
    public String createNewOption(@Valid OptionDTO optionDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("price"), "java.lang.NumberFormatException", priceDigitsMessage);
            model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());
            return templateFolder + "new_option";
        }
        try {
            optionsService.saveNewOption(optionDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());
            return templateFolder + "new_option";
        }
    }


    @GetMapping("${endpoints.admin.options.edit}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Option option = optionsService.getOptionById(id);
        OptionPlainView optionPlainView = new OptionPlainView(option);
        OptionDTO optionDTO = new OptionDTO(option);
        model.addAttribute("optionView", optionPlainView);
        model.addAttribute("optionDTO", optionDTO);
        model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());

        return templateFolder + "edit_option";
    }

    @PatchMapping("/{id}")
    public String updateOption(@PathVariable("id") int id, @Valid OptionDTO optionDTO,
                               BindingResult result, Model model) {
        if (result.hasErrors()) {
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("price"), "java.lang.NumberFormatException", priceDigitsMessage);
            Option option = optionsService.getOptionById(id);
            OptionPlainView optionPlainView = new OptionPlainView(option);
            model.addAttribute("optionView", optionPlainView);
            model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());
            return templateFolder + "edit_option";
        }
        try {
            optionsService.updateOption(id, optionDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("allGroups", optionGroupsService.findAllOptionGroupViews());

            Option option = optionsService.getOptionById(id);
            OptionPlainView optionPlainView = new OptionPlainView(option);
            model.addAttribute("optionView", optionPlainView);
            return templateFolder + "edit_option";
        }
    }

    @DeleteMapping("/{id}")
    public String deleteOption(@PathVariable("id") int id, Model model) {
        optionsService.deleteOption(id);
        return "redirect:" + controllerPath;
    }


}
