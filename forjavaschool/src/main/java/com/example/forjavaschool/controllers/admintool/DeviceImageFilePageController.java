package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.DeviceImage;
import com.example.forjavaschool.catalogmanagement.service.DeviceImageFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/admin/device_images")
public class DeviceImageFilePageController {

    @Autowired
    DeviceImageFileService fileStorageService;

    @Value("${endpoints.admin.device_images.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.device_images.new_upload}")
    private String newImagePath;
    @Value("${directory.templates.device_images}")
    private String templateFolder;

    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newImagePath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{name}");
        model.addAttribute("controllerPath", controllerPath);
    }

    @GetMapping
    public String getDeviceImagesPage(Model model) {
        List<DeviceImage> allDeviceImages = fileStorageService.getAllDeviceImages();
        model.addAttribute("allDeviceImages", allDeviceImages);
        return templateFolder + "device_image_files";
    }

    @GetMapping("${endpoints.admin.device_images.new_upload}")
    public String showNewDeviceImagePage() {
        return templateFolder + "new_device_image";
    }

    @PostMapping
    public String saveNewDeviceImage(@RequestParam("files") MultipartFile file, Model model) {
        String message = "";
        try {
            fileStorageService.save(file);
            return "redirect:" + controllerPath;

        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            System.out.println("!!!!!!!!!" + message);
            return templateFolder + "new_device_image";
        }
    }

    @DeleteMapping("/{name}")
    public String deleteDeviceImage(@PathVariable("name") String name, Model model) {
        fileStorageService.deleteFileWithDevicesLinks(name);
        return "redirect:" + controllerPath;

    }


    /*@PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            fileStorageService.save(file);

            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @GetMapping("/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = fileStorageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(DeviceImageFileController.class, "getFile", path.getFileName().toString()).build().toString();

            return new FileInfo(filename, url);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }
    */


    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<UrlResource> getFile(@PathVariable String filename) {
        UrlResource file = fileStorageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}

