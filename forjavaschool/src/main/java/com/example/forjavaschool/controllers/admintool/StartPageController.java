package com.example.forjavaschool.controllers.admintool;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class StartPageController {
    @Value("${server.servlet.context-path}")
    private String domain;
    @Value("${endpoints.admin.tariffs.controller_path}")
    private String tariffsControllerPath;
    @Value("${endpoints.admin.devices.controller_path}")
    private String devicesControllerPath;
    @Value("${endpoints.admin.offers.controller_path}")
    private String offersControllerPath;
    @Value("${endpoints.admin.option_groups.controller_path}")
    private String optionGroupsControllerPath;
    @Value("${endpoints.admin.options.controller_path}")
    private String optionsControllerPath;
    @Value("${endpoints.admin.device_images.controller_path}")
    private String deviceImagesControllerPath;


    @GetMapping("${endpoints.admin.startpage}")
    public String getStartPage(Model model) {
        model.addAllAttributes(Map.of("tariffsPage", domain + tariffsControllerPath,
                "devicesPage", domain + devicesControllerPath,
                "offersPage", domain + offersControllerPath,
                "optionGroupsPage", domain + optionGroupsControllerPath,
                "optionsPage", domain + optionsControllerPath,
                "deviceImagesPage", domain + deviceImagesControllerPath));
        return "startpage";
    }

}
