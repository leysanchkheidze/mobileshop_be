package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dto.DeviceDTO;
import com.example.forjavaschool.catalogmanagement.service.DevicesService;
import com.example.forjavaschool.catalogmanagement.viewmodels.DevicePlainView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("${endpoints.forshop.devices.controller_path}")
public class DevicesController {

    @Autowired
    DevicesService devicesService;

    @GetMapping("${endpoints.forshop.devices.all}")
    public ResponseEntity<List<DevicePlainView>> getAllDevicesPlain() {
        List<DevicePlainView> allDevices = devicesService.findAllDevicesPlain();

        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allDevices);
    }

    @PostMapping("${endpoints.forshop.devices.new}")
    public Map<String, Integer> createNewDevice(@RequestBody @Valid DeviceDTO deviceDTO) {
        Device newDevice = devicesService.saveNewDevice(deviceDTO);
        return Map.of("id", newDevice.getDeviceId());
    }

}
