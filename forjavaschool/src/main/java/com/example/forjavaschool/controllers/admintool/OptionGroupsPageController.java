package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.*;
import com.example.forjavaschool.catalogmanagement.dto.OptionGroupDTO;
import com.example.forjavaschool.catalogmanagement.dto.OptionListDTO;
import com.example.forjavaschool.catalogmanagement.service.EntityCannotBeSavedException;
import com.example.forjavaschool.catalogmanagement.service.OptionGroupsService;
import com.example.forjavaschool.catalogmanagement.service.OptionsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionGroupView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("${endpoints.admin.option_groups.controller_path}")
public class OptionGroupsPageController {
    @Autowired
    OptionGroupsService optionGroupsService;

    @Autowired
    OptionsService optionsService;

    @Value("${endpoints.admin.option_groups.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.option_groups.new}")
    private String newGroupPath;
    @Value("${endpoints.admin.option_groups.edit}")
    private String editGroupPath;
    @Value("${endpoints.admin.option_groups.edit_options}")
    private String editGroupOptionsPath;

    @Value("${directory.templates.option_groups}")
    private String templateFolder;


    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newGroupPath);
        model.addAttribute("pathEdit", controllerPath + editGroupPath);
        model.addAttribute("pathEditOptions", controllerPath + editGroupOptionsPath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{id}");
        model.addAttribute("controllerPath", controllerPath);
    }

    @GetMapping
    public String getOptionGroupsPage(Model model) {
        model.addAttribute("groups", optionGroupsService.findAllOptionGroupViews());
        return templateFolder + "optionGroups";
    }

    private void setModelForOptionGroupPages(Model model) {
        model.addAttribute("allOptions", optionsService.findAllOptionViews());
        model.addAttribute("groupTypes", OptionGroupType.values());
    }

    @GetMapping("${endpoints.admin.option_groups.new}")
    public String showNewOptionGroupPage(OptionGroupDTO groupDTO, Model model) {
        setModelForOptionGroupPages(model);
        return templateFolder + "new_option_group";
    }


    @PostMapping
    public String createNewGroup(@Valid OptionGroupDTO optionGroupDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            setModelForOptionGroupPages(model);
            return templateFolder + "new_option_group";
        }
        try {
            optionGroupsService.saveNewGroup(optionGroupDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setModelForOptionGroupPages(model);
            return templateFolder + "new_option_group";
        }
    }


    @GetMapping("${endpoints.admin.option_groups.edit}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        OptionGroup optionGroup = optionGroupsService.getOptionGroupById(id);
        OptionGroupView groupView = new OptionGroupView(optionGroup);
        OptionGroupDTO groupDTO = new OptionGroupDTO(optionGroup);
        model.addAttribute("groupView", groupView);
        model.addAttribute("groupDTO", groupDTO);
        setModelForOptionGroupPages(model);
        return templateFolder + "edit_option_group";
    }

    @PatchMapping("/{id}")
    public String updateGroupFields(@PathVariable("id") int id, @Valid OptionGroupDTO groupDTO,
                                    BindingResult result, Model model) {
        System.out.println("starting to update group fields");
        //TODO: Боря, я не понимаю, у меня при валидационной ошибке в этом контроллере валится:
        //[04-02-2022T19:22:42.042Z] [DEBUG] [org.springframework.web.servlet.DispatcherServlet] [http-nio-8080-exec-9] [1405] Error rendering view [org.thymeleaf.spring5.view.ThymeleafView@59f4bb5]
        //org.thymeleaf.exceptions.TemplateProcessingException: Error during execution of processor 'org.thymeleaf.spring5.processor.SpringInputGeneralFieldTagProcessor' (template: "edit_option_group" - line 31, col 32)
        //TODO: в аналогичном девайс пейдж контроллере все ок, и я не могу найти разницу
        if (result.hasErrors()) {
            setModelForOptionGroupPages(model);
            OptionGroup optionGroup = optionGroupsService.getOptionGroupById(id);
            OptionGroupView groupView = new OptionGroupView(optionGroup);
            model.addAttribute("groupView", groupView);
            return templateFolder + "edit_option_group";

        }
        try {
            optionGroupsService.updateOptionGroupFields(id, groupDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setModelForOptionGroupPages(model);
            OptionGroup optionGroup = optionGroupsService.getOptionGroupById(id);
            OptionGroupView groupView = new OptionGroupView(optionGroup);
            model.addAttribute("groupView", groupView);
            return templateFolder + "edit_option_group";
        }
    }

    private void setModelForEditOptionsInGroup(int groupId, Model model) {
        OptionGroup optionGroup = optionGroupsService.getOptionGroupById(groupId);
        OptionGroupView groupView = new OptionGroupView(optionGroup);
        OptionListDTO optionListDTO = OptionListDTO.getOptionListDTO(optionGroup);
        List<OptionPlainView> allOptions = optionsService.findAllOptionViews();
        Set<OptionPlainView> currentOptions = optionGroupsService.getGroupOptionViews(optionGroup);

        model.addAttribute("groupView", groupView);
        model.addAttribute("optionListDTO", optionListDTO);
        model.addAttribute("allOptions", allOptions);
        model.addAttribute("currentOptions", currentOptions);
    }


    @GetMapping("${endpoints.admin.option_groups.edit_options}")
    public String showUpdateOptionsForm(@PathVariable("id") int id, Model model) {
        setModelForEditOptionsInGroup(id, model);
        return templateFolder + "edit_options_in_group";
    }

    @PatchMapping("${endpoints.admin.option_groups.edit_options}")
    public String updateOptionsInGroup(@PathVariable("id") int id, @Valid OptionListDTO optionListDTO,
                                       BindingResult result, Model model) {
        if (result.hasErrors()) {
            setModelForEditOptionsInGroup(id, model);
            return templateFolder + "edit_option_group";

        }
        try {
            OptionGroup group = optionGroupsService.getOptionGroupById(id);
            optionGroupsService.updateOptionsInGroup(group, optionListDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setModelForEditOptionsInGroup(id, model);
            return templateFolder + "edit_option_group";
        }
    }


    @DeleteMapping("/{id}")
    public String deleteGroup(@PathVariable("id") int id, Model model) {
        optionGroupsService.deleteGroup(id);
        return "redirect:" + controllerPath;
    }

}
