package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.DeviceImage;
import com.example.forjavaschool.catalogmanagement.dto.DeviceDTO;
import com.example.forjavaschool.catalogmanagement.service.DeviceImageFileService;
import com.example.forjavaschool.catalogmanagement.service.DevicesService;
import com.example.forjavaschool.catalogmanagement.service.EntityCannotBeSavedException;
import com.example.forjavaschool.catalogmanagement.viewmodels.DevicePlainView;
import com.example.forjavaschool.utils.BindingResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@PropertySource("messages.properties")
@RequestMapping("${endpoints.admin.devices.controller_path}")
public class DevicesPagesController {

    @Value("${endpoints.admin.devices.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.devices.new}")
    private String newDevicePath;
    @Value("${endpoints.admin.devices.edit}")
    private String editDevicePath;
    @Value("${directory.templates.devices}")
    private String templateFolder;
    @Value("${general.price.nonnumber.msg}")
    private String priceDigitsMessage;


    @Autowired
    DevicesService devicesService;

    @Autowired
    DeviceImageFileService deviceImageFileService;

    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newDevicePath);
        model.addAttribute("pathEdit", controllerPath + editDevicePath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{id}");
        model.addAttribute("controllerPath", controllerPath);
    }

    @GetMapping
    public String getDevicesPage(Model model) {
        model.addAttribute("devices", devicesService.findAllDevicesPlain());
        return templateFolder + "devices";
    }

    private void setModelForDevicePage(Model model) {
        List<DeviceImage> uploadedImages = deviceImageFileService.getAllDeviceImages();
        model.addAttribute("images", uploadedImages);
    }

    @GetMapping("${endpoints.admin.devices.new}")
    public String showNewDevicePage(DeviceDTO deviceDTO, Model model) {
        setModelForDevicePage(model);
        return templateFolder + "new_device";
    }

    @PostMapping
    public String createNewDevice(@Valid DeviceDTO deviceDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            setModelForDevicePage(model);
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("soloPrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            return templateFolder + "new_device";
        }
        try {
            devicesService.saveNewDevice(deviceDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setModelForDevicePage(model);
            return templateFolder + "new_device";
        }
    }

    @GetMapping("${endpoints.admin.devices.edit}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        DeviceDTO deviceDTO = new DeviceDTO(devicesService.getDeviceById(id));
        DevicePlainView deviceView = devicesService.getDeviceViewByID(id);
        setModelForDevicePage(model);
        model.addAttribute("deviceDTO", deviceDTO);
        model.addAttribute("deviceView", deviceView);
        return templateFolder + "edit_device";
    }

    @PatchMapping("/{id}")
    public String updateDevice(@PathVariable("id") int id, @Valid DeviceDTO deviceDTO,
                               BindingResult result, Model model) {
        System.out.println("staring update device");
        if (result.hasErrors()) {
            setModelForDevicePage(model);
            DevicePlainView deviceView = devicesService.getDeviceViewByID(id);
            model.addAttribute("deviceView", deviceView);
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("soloPrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            return templateFolder + "edit_device";
        }

        try {
            devicesService.updateDevice(id, deviceDTO);
            return "redirect:" + controllerPath;

        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            DevicePlainView deviceView = devicesService.getDeviceViewByID(id);
            model.addAttribute("deviceView", deviceView);
            setModelForDevicePage(model);
            return templateFolder + "edit_device";
        }
    }

    @DeleteMapping("/{id}")
    public String deleteDevice(@PathVariable("id") int id, Model model) {
        devicesService.deleteDeviceByID(id);
        return "redirect:" + controllerPath;

    }


}
