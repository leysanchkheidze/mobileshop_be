package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.ordermanagement.dao.*;
import com.example.forjavaschool.ordermanagement.dto.AddressDTO;
import com.example.forjavaschool.ordermanagement.dto.OrderConfigurationItemsDTO;
import com.example.forjavaschool.ordermanagement.dto.PassportDataDTO;
import com.example.forjavaschool.ordermanagement.dto.PersonalDataDTO;
import com.example.forjavaschool.catalogmanagement.service.OrdersService;
import com.example.forjavaschool.ordermanagement.viewmodels.OrderView;
import com.example.forjavaschool.ordermanagement.viewmodels.PassportTypeView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${endpoints.forshop.orders.controller_path}")
public class OrdersController {

    @Autowired
    OrdersService ordersService;
    @Autowired
    Order order;

    @PostMapping
    public ResponseEntity<OrderView> createNewOrder(@RequestBody @Valid OrderConfigurationItemsDTO orderConfigurationItemsDTO) {
        OrderConfigurationItems orderConfigurationItems = ordersService.getOrderConfigItemsFromDTO(orderConfigurationItemsDTO);
        order = ordersService.getOrderFromOrderConfigItems(orderConfigurationItems);
        OrderView view = new OrderView(order);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(view);
    }

    //TODO: переделать на пут (и уточнить у Бори, почему не патч)
    @PostMapping("${endpoints.forshop.orders.address}")
    public ResponseEntity<OrderView> setAddress(@RequestBody @Valid AddressDTO addressDTO) {
        order.setAddress(new Address(addressDTO));
        OrderView view = new OrderView(order);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(view);
    }

    @PostMapping("${endpoints.forshop.orders.personal_data}")
    public ResponseEntity<OrderView> setPersonalData(@RequestBody @Valid PersonalDataDTO personalDataDTO) {
        PersonalData personalData = new PersonalData(personalDataDTO);
        order.setPersonalData(personalData);
        OrderView view = new OrderView(order);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(view);
    }

    @PostMapping("${endpoints.forshop.orders.passport_data}")
    public ResponseEntity<OrderView> setPassportData(@RequestBody @Valid PassportDataDTO passportDataDTO) {
        PassportData passportData = new PassportData(passportDataDTO);
        order.setPassportData(passportData);
        OrderView view = new OrderView(order);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(view);
    }

    @GetMapping("${endpoints.forshop.orders.passport_types}")
    public ResponseEntity<List<PassportTypeView>> getPassportTypes() {
     //   List<PassportType> allTypes = Arrays.asList(PassportType.values());
        List<PassportTypeView> allViews = Arrays.stream(PassportType.values())
                .map(PassportTypeView::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allViews);
    }
}
