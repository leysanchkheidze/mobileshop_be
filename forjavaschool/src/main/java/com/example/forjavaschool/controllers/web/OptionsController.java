package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dto.OptionDTO;
import com.example.forjavaschool.catalogmanagement.service.OptionsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${endpoints.forshop.options.controller_path}")
public class OptionsController {

    @Autowired
    OptionsService optionsService;

    @GetMapping("${endpoints.forshop.options.all}")
    public ResponseEntity<List<OptionPlainView>> getAllOptionsPlain() {
        List<OptionPlainView> allOptions = optionsService.findAllOptionViews();

        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allOptions);
    }

    @PostMapping("${endpoints.forshop.options.new}")
    public Map<String, Integer> createNewOption(@RequestBody @Valid OptionDTO optionDTO) {
        Option option = optionsService.saveNewOption(optionDTO);
        return Map.of("id", option.getId());
    }
}
