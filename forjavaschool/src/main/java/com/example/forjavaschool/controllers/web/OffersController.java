package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dto.OfferDTO;
import com.example.forjavaschool.ordermanagement.viewmodels.OrderConfigurationView;
import com.example.forjavaschool.catalogmanagement.service.OffersService;
import com.example.forjavaschool.catalogmanagement.viewmodels.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${endpoints.forshop.offers.controller_path}")
public class OffersController {

    @Autowired
    OffersService offersService;


    @GetMapping("${endpoints.forshop.offers.all}")
    public ResponseEntity<List<OfferView>> getAllOffers() {
        List<OfferView> allOffers = offersService.findAllOfferViews();

        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allOffers);
    }

    @PostMapping("${endpoints.forshop.offers.new}")
    public Map<String, Integer> createNewOffer(@RequestBody @Valid OfferDTO offerDTO) {
        Offer newOffer = offersService.saveOffer(offerDTO);
        return Map.of("id", newOffer.getOfferId());
    }

    /**
     * Returns compatible devices view with selected tariff
     *
     * @param id - tariff id for which devices are found
     * @return CompatibleDevicesView. Contains selected TariffView and list of compatible DeviceView's
     */
    @GetMapping("${endpoints.forshop.offers.by_tariff}")
    public ResponseEntity<CompatibleDevicesView> getCompatibleDevices(@PathVariable("id") int id) {
        CompatibleDevicesView result = offersService.getCompatibleDeviceView(id);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(result);
    }

    /**
     * Returns compatible tariffs view with selected device
     *
     * @param id - device id for which tariffs are found
     * @return CompatibleTariffsView. Contains selected DeviceView and list of compatible TariffView's
     */
    @GetMapping("${endpoints.forshop.offers.by_device}")
    public ResponseEntity<CompatibleTariffsView> getCompatibleTariffs(@PathVariable("id") int id) {
        CompatibleTariffsView result = offersService.getCompatibleTariffView(id);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(result);
    }

    @GetMapping("${endpoints.forshop.offers.order_configuration}")
    public ResponseEntity<OrderConfigurationView> getOrderConfigurationView(@PathVariable("offerId") int offerID) {
        OrderConfigurationView view = offersService.getOrderConfiguration(offerID);
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(view);
    }


}
