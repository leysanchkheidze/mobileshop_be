package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.catalogmanagement.dto.OptionListDTO;
import com.example.forjavaschool.catalogmanagement.dto.TariffDTO;
import com.example.forjavaschool.catalogmanagement.service.EntityCannotBeSavedException;
import com.example.forjavaschool.catalogmanagement.service.OptionsService;
import com.example.forjavaschool.catalogmanagement.service.TariffsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffCompatibleOptionsView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import com.example.forjavaschool.utils.BindingResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@PropertySource("messages.properties")
@RequestMapping("${endpoints.admin.tariffs.controller_path}")
public class TariffsPagesController {

    @Autowired
    TariffsService tariffsService;

    @Autowired
    OptionsService optionsService;

    @Value("${endpoints.admin.tariffs.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.tariffs.new}")
    private String newTariffPath;
    @Value("${endpoints.admin.tariffs.edit}")
    private String editTariffPath;
    @Value("${endpoints.admin.tariffs.edit_options}")
    private String editTariffOptionsPath;
    @Value("${general.price.nonnumber.msg}")
    private String priceDigitsMessage;

    @Value("${directory.templates.tariffs}")
    private String templateFolder;

    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newTariffPath);
        model.addAttribute("pathEdit", controllerPath + editTariffPath);
        model.addAttribute("pathEditOptions", controllerPath + editTariffOptionsPath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{id}");
        model.addAttribute("controllerPath", controllerPath);
    }


    @GetMapping
    public String getTariffsPage(Model model) {
        model.addAttribute("tariffs", tariffsService.findAllTariffsPlain());
        return templateFolder + "tariffs";
    }


    @GetMapping("${endpoints.admin.tariffs.new}")
    public String showNewTariffPage(TariffDTO tariffDTO) {
        return templateFolder + "new_tariff";
    }


    @PostMapping
    public String createNewTariff(@Valid TariffDTO tariffDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("monthlyPrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            return templateFolder + "new_tariff";
        }
        try {
            tariffsService.saveNewTariff(tariffDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            return templateFolder + "new_tariff";
        }

    }

    @GetMapping("${endpoints.admin.tariffs.edit}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        TariffPlainView tariffView = tariffsService.getTariffPlainViewByID(id);
        model.addAttribute("tariff", tariffView);
        TariffDTO tariffDTO = new TariffDTO(tariffView);
        model.addAttribute("tariffDTO", tariffDTO);
        return templateFolder + "edit_tariff";
    }

    @PatchMapping("/{id}")
    public String updateTariff(@PathVariable("id") int id, @Valid TariffDTO tariffDTO,
                               BindingResult result, Model model) {
        if (result.hasErrors()) {
            TariffPlainView tariffView = tariffsService.getTariffPlainViewByID(id);
            model.addAttribute("tariff", tariffView);
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("monthlyPrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            return templateFolder + "edit_tariff";
        }
        try {
            tariffsService.updateTariff(id, tariffDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            TariffPlainView tariffView = tariffsService.getTariffPlainViewByID(id);
            model.addAttribute("tariff", tariffView);
            return templateFolder + "edit_tariff";
        }

    }

    private void setEditTariffOptionsPageModel(Model model, int id) {
        Tariff tariff = tariffsService.getTariffById(id);
        TariffCompatibleOptionsView tariffOptionsView = new TariffCompatibleOptionsView(tariff);
        model.addAttribute("tariffOptionsView", tariffOptionsView);
        List<OptionPlainView> allOptions = optionsService.findAllOptionViews();
        model.addAttribute("allOptions", allOptions);
        OptionListDTO optionListDTO = OptionListDTO.getOptionListDTO(tariffOptionsView);
        model.addAttribute("optionListDTO", optionListDTO);
    }

    @GetMapping("${endpoints.admin.tariffs.edit_options}")
    public String showUpdateCompatibleOptionsForm(@PathVariable("id") int id, Model model) {
        setEditTariffOptionsPageModel(model, id);
        return templateFolder + "edit_tariff_options";
    }

    @PatchMapping("${endpoints.admin.tariffs.edit_options}")
    public String updateCompatibleOptions(@PathVariable("id") int id, @Valid OptionListDTO optionListDTO,
                                          BindingResult result, Model model) {
        if (result.hasErrors()) {
            setEditTariffOptionsPageModel(model, id);
            return templateFolder + "edit_tariff_options";
        }
        try {
            tariffsService.updateCompatibleOptionsInTariff(id, optionListDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setEditTariffOptionsPageModel(model, id);
            return templateFolder + "edit_tariff_options";
        }
    }


    @DeleteMapping("/{id}")
    public String deleteTariff(@PathVariable("id") int id, Model model) {
        tariffsService.deleteTariffByID(id);
        return "redirect:" + controllerPath;
    }


}
