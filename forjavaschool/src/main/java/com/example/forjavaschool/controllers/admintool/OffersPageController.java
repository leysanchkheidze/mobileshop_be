package com.example.forjavaschool.controllers.admintool;

import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dto.OfferDTO;
import com.example.forjavaschool.catalogmanagement.service.DevicesService;
import com.example.forjavaschool.catalogmanagement.service.EntityCannotBeSavedException;
import com.example.forjavaschool.catalogmanagement.service.OffersService;
import com.example.forjavaschool.catalogmanagement.service.TariffsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.DevicePlainView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OfferView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import com.example.forjavaschool.utils.BindingResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@PropertySource("messages.properties")
@RequestMapping("${endpoints.admin.offers.controller_path}")
public class OffersPageController {
    @Autowired
    OffersService offersService;
    @Autowired
    TariffsService tariffsService;
    @Autowired
    DevicesService devicesService;

    @Value("${endpoints.admin.offers.controller_path}")
    private String controllerPath;
    @Value("${endpoints.admin.offers.new}")
    private String newOfferPath;
    @Value("${endpoints.admin.offers.edit}")
    private String editOfferPath;
    @Value("${directory.templates.offers}")
    private String templateFolder;
    @Value("${general.price.nonnumber.msg}")
    private String priceDigitsMessage;


    @ModelAttribute
    public void setPathsAttributes(Model model) {
        model.addAttribute("pathNew", controllerPath + newOfferPath);
        model.addAttribute("pathEdit", controllerPath + editOfferPath);
        model.addAttribute("pathDeleteUpdate", controllerPath + "/{id}");
        model.addAttribute("controllerPath", controllerPath);
    }

    @GetMapping
    public String getOffersPage(Model model) {
        model.addAttribute("offersViews", offersService.findAllOfferViews());
        return templateFolder + "offers";
    }

    private void setOfferPageModel(Model model) {
        List<TariffPlainView> allTariffs = tariffsService.findAllTariffsPlain();
        model.addAttribute("allTariffs", allTariffs);
        List<DevicePlainView> allDevices = devicesService.findAllDevicesPlain();
        model.addAttribute("allDevices", allDevices);
    }

    @GetMapping("${endpoints.admin.offers.new}")
    public String showNewOfferPage(OfferDTO offerDTO, Model model) {
        setOfferPageModel(model);
        return templateFolder + "new_offer";
    }


    @PostMapping
    public String createNewOffer(@Valid OfferDTO offerDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            setOfferPageModel(model);
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("devicePrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            return templateFolder + "new_offer";
        }
        try {
            offersService.saveOffer(offerDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            setOfferPageModel(model);
            return templateFolder + "new_offer";
        }
    }


    @GetMapping("${endpoints.admin.offers.edit}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Offer offer = offersService.getOfferById(id);
        OfferView offerView = new OfferView(offer);
        OfferDTO offerDTO = new OfferDTO(offer.getTariff().getTariffId(), offer.getDevice().getDeviceId(),
                offer.getDevicePrice());
        setOfferPageModel(model);
        model.addAttribute("offerDTO", offerDTO);
        model.addAttribute("offerView", offerView);
        return templateFolder + "edit_offer";
    }

    @PatchMapping("/{id}")
    public String updateOffer(@PathVariable("id") int id, @Valid OfferDTO offerDTO,
                              BindingResult result, Model model) {
        if (result.hasErrors()) {
            BindingResultUtils.setNiceValidationMessages(model, result, List.of("devicePrice"), "java.lang.NumberFormatException", priceDigitsMessage);
            OfferView offerView = new OfferView(offersService.getOfferById(id));
            model.addAttribute("offerView", offerView);
            setOfferPageModel(model);
            return templateFolder + "edit_offer";
        }
        try {
            offersService.updateOffer(id, offerDTO);
            return "redirect:" + controllerPath;
        } catch (EntityCannotBeSavedException e) {
            model.addAttribute("errorEntity", e.getEntityName());
            model.addAttribute("errorMessage", e.getMessage());
            OfferView offerView = new OfferView(offersService.getOfferById(id));
            model.addAttribute("offerView", offerView);
            setOfferPageModel(model);
            return templateFolder + "edit_offer";
        }
    }

    @DeleteMapping("/{id}")
    public String deleteOffer(@PathVariable("id") int id, Model model) {
        offersService.deleteOfferByID(id);
        return "redirect:" + controllerPath;
    }
}
