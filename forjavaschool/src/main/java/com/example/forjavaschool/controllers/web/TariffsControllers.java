package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.catalogmanagement.dto.TariffDTO;
import com.example.forjavaschool.catalogmanagement.service.TariffsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${endpoints.forshop.tariffs.controller_path}")
public class TariffsControllers {

    @Autowired
    TariffsService tariffsService;


    @GetMapping("${endpoints.forshop.tariffs.all}")
    public ResponseEntity<List<TariffPlainView>> getAllTariffsPlain() {
        List<TariffPlainView> allTariffs = tariffsService.findAllTariffsPlain();

        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allTariffs);
    }


    @PostMapping("${endpoints.forshop.tariffs.new}")
    public Map<String, Integer> createNewTariff(@RequestBody @Valid TariffDTO tariffDTO) {
        Tariff newTariff = tariffsService.saveNewTariff(tariffDTO);
        return Map.of("id", newTariff.getTariffId());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
