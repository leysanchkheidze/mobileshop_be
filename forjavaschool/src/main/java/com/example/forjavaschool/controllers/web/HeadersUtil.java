package com.example.forjavaschool.controllers.web;

import org.springframework.http.HttpHeaders;

public class HeadersUtil {

    public static HttpHeaders appJsonHeaders() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type",
                "application/json");
        return responseHeaders;
    }
}
