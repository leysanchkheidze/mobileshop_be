package com.example.forjavaschool.controllers.web;

import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.dto.OptionGroupDTO;
import com.example.forjavaschool.catalogmanagement.service.OptionGroupsService;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionGroupView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${endpoints.forshop.option_groups.controller_path}")
public class OptionGroupsController {

    @Autowired
    OptionGroupsService optionGroupsService;


    @GetMapping("${endpoints.forshop.option_groups.all}")
    public ResponseEntity<List<OptionGroupView>> getAllOptionsGroups() {
        List<OptionGroupView> allGroupViews = optionGroupsService.findAllOptionGroupViews();
        return ResponseEntity.ok()
                .headers(HeadersUtil.appJsonHeaders())
                .body(allGroupViews);
    }

    @PostMapping("${endpoints.forshop.option_groups.new}")
    public Map<String, Integer> createNewOption(@RequestBody @Valid OptionGroupDTO groupDTO) {
        OptionGroup group = optionGroupsService.saveNewGroup(groupDTO);
        optionGroupsService.saveEveryOptionInGroup(group);
        return Map.of("id", group.getId());
    }
}
