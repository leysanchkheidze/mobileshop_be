package com.example.forjavaschool;

import com.example.forjavaschool.catalogmanagement.service.DeviceImageFileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

@SpringBootApplication
public class ForjavaschoolApplication implements CommandLineRunner {
	public static final Logger globalLogger = LoggerFactory.getLogger("Global logger");

	//TODO: переделать апплиейшн пропертис в ямл

	@Resource
	DeviceImageFileService deviceImageFileServiceService;


	public static void main(String[] args) {
		SpringApplication.run(ForjavaschoolApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		deviceImageFileServiceService.init();
	}



}
