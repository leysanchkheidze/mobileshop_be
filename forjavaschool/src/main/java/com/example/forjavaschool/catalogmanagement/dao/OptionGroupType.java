package com.example.forjavaschool.catalogmanagement.dao;

public enum OptionGroupType {
    ALTERNATIVE_GROUP(1, "Alternative group"),
    SELECTION_GROUP(2, "Selection group"),
    SINGLE_GROUP(0, "Single group");


    public int typeIndex;
    public String name;

    OptionGroupType(int typeIndex, String name) {
        this.typeIndex = typeIndex;
        this.name = name;
    }

    public static OptionGroupType getByIndex(Integer index) {
        if (null == index) {
            return SINGLE_GROUP;
        }
        for (OptionGroupType type : values()) {
            if (type.typeIndex == index) {
                return type;
            }
        }
        return SINGLE_GROUP;
    }

    public static int getIndex(OptionGroupType type) {
        return type.typeIndex;
    }

}
