package com.example.forjavaschool.catalogmanagement.viewmodels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@ToString
public class CompatibleDevicesView {
    TariffPlainView tariffPlainViewView;
    List<DeviceInOfferView> deviceInOfferViewList;
    boolean success;


    public static CompatibleDevicesView getNotFoundResult(int tariffId, Exception e) {
        return new CompatibleDevicesView(TariffPlainView.getNotFoundTariff(tariffId, e.getMessage()), new ArrayList<>(), false);
    }
}
