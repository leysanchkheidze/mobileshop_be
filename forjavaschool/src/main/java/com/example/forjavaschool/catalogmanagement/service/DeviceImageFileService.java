package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.controllers.admintool.DeviceImageFilePageController;
import com.example.forjavaschool.catalogmanagement.dao.DeviceImage;
import com.example.forjavaschool.catalogmanagement.repositories.DeviceImageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static com.example.forjavaschool.ForjavaschoolApplication.globalLogger;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class DeviceImageFileService {
    @Value("${directory.upload.deviceImageFolder}")
    private String uploadDirectory;

    @Autowired
    DevicesService devicesService;

    @Autowired
    DeviceImageRepo deviceImageRepo;

    private static final Path root = Paths.get("uploads");
    private static final String defaultFileName = "default.jpg";

    public void init() {
        if (!Files.isDirectory(root)) {
            try {
                Files.createDirectory(root);
            } catch (IOException e) {
                throw new RuntimeException("Could not initialize folder for upload!");
            }
        }
    }

    public void save(MultipartFile file) throws IOException {
        try {
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()), REPLACE_EXISTING);

            DeviceImage image = new DeviceImage();
            image.setFileName(file.getOriginalFilename());
            UriComponentsBuilder builder = MvcUriComponentsBuilder
                    .fromMethodName(DeviceImageFilePageController.class, "getFile", file.getOriginalFilename());
            String url = builder.build().toString();
            image.setImageURL(url);
            deviceImageRepo.save(image);

        } catch (IOException e) {
            globalLogger.warn("IllegalArgumentException during saving file " + file.getOriginalFilename());
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            //can be thrown during creating a builder, building URK of saving in repo:
            globalLogger.warn("IllegalArgumentException during saving file " + file.getOriginalFilename());
            //delete the file from server:
            Path filePath = root.resolve(file.getOriginalFilename());
            Files.delete(filePath);

        }
    }

    @Transactional
    private void deleteFile(String filename) {
        try {
            Path file = root.resolve(filename);
            Files.delete(file);
            deviceImageRepo.deleteByFileName(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void deleteFileWithDevicesLinks(String filename) {
        devicesService.findAllDevicesPlain()
                .stream()
                .filter(devicePlainView -> devicePlainView.getImageFileName().equals(filename))
                .findFirst()
                .ifPresent(deviceWithFile -> devicesService.removeImageLink(deviceWithFile.getDeviceId()));

        deleteFile(filename);
    }

    public UrlResource load(String filename) {
        try {
            Path file = root.resolve(filename);
            UrlResource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }

    public List<DeviceImage> getAllDeviceImages() {
        List<DeviceImage> images = deviceImageRepo.findAll();
        Collections.sort(images);
        return images;
    }

    public static String getDefaultImageURL() {
        //TODO: наверное, хорошо бы брать из базы, но тогда нельзя сделать статик метод
        //используется в DevicePlainView, и тащить туда инстанс этого сервиса - наверное лишнее. Как сделать?
        Path defaultFile = Path.of(root + "/" + defaultFileName);
        if (Files.isReadable(defaultFile)) {
            UriComponentsBuilder builder = MvcUriComponentsBuilder
                    .fromMethodName(DeviceImageFilePageController.class, "getFile", defaultFile.getFileName().toString());
            String url = builder.build().toString();
            return url;
        } else {
            return "";
        }
    }


}
