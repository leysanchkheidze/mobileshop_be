package com.example.forjavaschool.catalogmanagement.dao;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;


@Getter
@Setter
@Entity
@Table(name = "OFFERS", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"tariff_id", "device_id"})
})
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "OFFER_ID")
    private int offerId;

    @Column(name = "DEVICE_PRICE")
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 4, fraction = 2)
    @NotNull
    private BigDecimal devicePrice;

    @ManyToOne
    @JoinColumn(name = "TARIFF_ID")
    private Tariff tariff;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;


    public Offer() {
    }

    public Offer(Tariff tariff, Device device, BigDecimal devicePrice) {
        this.tariff = tariff;
        this.device = device;
        this.devicePrice = devicePrice;
    }

    @Override
    public String toString() {
        return String.format("Offer[id: %d, device price: %s, tariff: %s, device: %s %s]",
                offerId, devicePrice, tariff.getTariffName(), device.getBrand(), device.getModel());
    }

}
