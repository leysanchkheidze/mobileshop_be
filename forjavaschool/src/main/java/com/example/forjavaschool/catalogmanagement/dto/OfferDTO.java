package com.example.forjavaschool.catalogmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@ToString
public class OfferDTO {

    @NotNull(message = "{offerdto.tariff.null.msg}")
    private Integer tariffId;
    @NotNull(message = "{offerdto.device.null.msg}")
    private Integer deviceId;
    @NotNull(message = "{general.price.null.msg}")
    @Digits(integer = 5, fraction = 2, message = "{devicedto.price.size.msg}")
    @PositiveOrZero(message = "{general.price.negative.msg}")
    private BigDecimal devicePrice;

    //Todo: если убрать ломбоковские геттеры и оставить свои, то при открытии страницы создания оффера
    // при вызове геттеров падает нпе: разобраться как это работает
   /*public int getTariffId() {
        return tariffId;
    }*/

   /* public int getDeviceId() {
        return deviceId;
    }

    public BigDecimal getDevicePrice() {
        return devicePrice;
    }*/

    /*public OfferDTO(){
    }

    public OfferDTO(int tariffId, int deviceId, BigDecimal soloPrice) {
        this.tariffId = tariffId;
        this.deviceId = deviceId;
        this.devicePrice = soloPrice;
    }*/

    /*public OfferDTO(Offer offer) {
        this.tariffId = offer.getTariff().getTariffId();
        this.deviceId = offer.getDevice().getDeviceId();
        this.devicePrice = offer.getDevicePrice();
    }*/

  }
