package com.example.forjavaschool.catalogmanagement.service;

import org.springframework.dao.DataIntegrityViolationException;

public class EntityCannotBeSavedException extends DataIntegrityViolationException {
    private String entityName;


    public EntityCannotBeSavedException(String msg) {
        super(msg);
    }

    public EntityCannotBeSavedException(String entityName, String msg) {
        this(msg);
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getMessage() {
        return super.getMessage();
    }
}
