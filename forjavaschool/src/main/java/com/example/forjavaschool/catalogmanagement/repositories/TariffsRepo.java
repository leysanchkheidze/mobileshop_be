package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TariffsRepo extends JpaRepository<Tariff, Integer> {

    public boolean existsByTariffName(String tariffName);
}
