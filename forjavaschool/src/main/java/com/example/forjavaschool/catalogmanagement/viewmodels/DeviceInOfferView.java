package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.service.DeviceImageFileService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Comparator;

@AllArgsConstructor
@Getter
public class DeviceInOfferView implements Comparable<DeviceInOfferView> {
    private Integer deviceId;
    private String brand;
    private String model;
    private String description;
    private BigDecimal soloPrice;
    private String imageURI;
    private String imageFileName;
    private BigDecimal priceWithTariff;
    private Integer offerId;


    public DeviceInOfferView(Device device, Offer offer) {
        this.deviceId = device.getDeviceId();
        this.brand = device.getBrand();
        this.model = device.getModel();
        this.description = device.getDescription();
        this.soloPrice = device.getSoloPrice();
        if (device.getDeviceImage() != null) {
            this.imageURI = device.getDeviceImage().getImageURL();
            this.imageFileName = device.getDeviceImage().getFileName();
        } else {
            this.imageFileName = "default";
            this.imageURI = DeviceImageFileService.getDefaultImageURL();
        }
        this.priceWithTariff = offer.getDevicePrice();
        this.offerId = offer.getOfferId();
    }

    public static DeviceInOfferView getNotFoundDeviceView(int deviceId, String model) {
        return new DeviceInOfferView(deviceId, null, model, null, null, null, null, null, null);
    }

    @Override
    public int compareTo(DeviceInOfferView o) {
        return Comparator.comparing(DeviceInOfferView::getBrand)
                .thenComparing(DeviceInOfferView::getModel)
                .compare(this, o);
    }

}
