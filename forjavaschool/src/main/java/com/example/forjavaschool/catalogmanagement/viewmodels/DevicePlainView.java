package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.service.DeviceImageFileService;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Comparator;

@Getter
@Setter
@ToString
public class DevicePlainView implements Comparable<DevicePlainView> {

    private Integer deviceId;
    private String brand;
    private String model;
    private String description;
    private BigDecimal soloPrice;
    // private String imageURI;
    // private FileInfo imageFileInfo;
    private String imageFileName;
    private String imageURL;


    public DevicePlainView(Integer deviceId, String brand, String model, String description, BigDecimal soloPrice, String imageFileName, String imageURL) {
        this.deviceId = deviceId;
        this.brand = brand;
        this.model = model;
        this.description = description;
        this.soloPrice = soloPrice;
        this.imageFileName = imageFileName;
        this.imageURL = imageURL;

    }

    public DevicePlainView(Device device) {
        this.deviceId = device.getDeviceId();
        this.brand = device.getBrand();
        this.model = device.getModel();
        this.description = device.getDescription();
        this.soloPrice = device.getSoloPrice();
        if (device.getDeviceImage() == null) {
            this.imageFileName = "default";
            this.imageURL = DeviceImageFileService.getDefaultImageURL();
        } else {
            this.imageFileName = device.getDeviceImage().getFileName();
            this.imageURL = device.getDeviceImage().getImageURL();
        }
    }

    public static DevicePlainView getNotFoundDeviceView(int deviceId, String model) {
        return new DevicePlainView(deviceId, null, model, null, null, "default", "null");
    }


    @Override
    public int compareTo(DevicePlainView o) {
        return Comparator.comparing(DevicePlainView::getBrand)
                .thenComparing(DevicePlainView::getModel)
                .compare(this, o);
    }
}
