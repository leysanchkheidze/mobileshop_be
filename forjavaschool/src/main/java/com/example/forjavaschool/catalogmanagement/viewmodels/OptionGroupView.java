package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroupType;
import lombok.*;

import java.net.URI;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class OptionGroupView implements DisplayableOptionSelection, Comparable<OptionGroupView> {
    private int id;
    private String groupName;
    private String description;
    private URI iconURI;
    private Set<OptionPlainView> options;
    private OptionGroupType groupType;

    public OptionGroupView(int id, String groupName, String description, URI iconURI, OptionGroupType groupType, Set<OptionPlainView> options) {
        this.id = id;
        this.groupName = groupName;
        this.description = description;
        this.iconURI = iconURI;
        this.groupType = groupType;
        this.options = options;

    }

    public OptionGroupView(OptionGroup group) {
        this.id = group.getId();
        this.groupName = group.getName();
        this.description = group.getDescription();
        this.iconURI = group.getIconURI();
        this.groupType = group.getGroupType();
        this.options = group.getOptions().stream()
                .map(OptionPlainView::new)
                .collect(Collectors.toSet());
    }

    public static final OptionGroupView EMPTY_GROUP_VIEW = new OptionGroupView(
            -1,
            "",
            "",
            URI.create(""),
            OptionGroupType.SINGLE_GROUP,
            Collections.emptySet());

    @Override
    public OptionSelectionType getType() {
        return OptionSelectionType.GROUP;
    }

    @Override
    public int compareTo(OptionGroupView o) {
        return groupName.compareTo(o.getGroupName());
    }
}
