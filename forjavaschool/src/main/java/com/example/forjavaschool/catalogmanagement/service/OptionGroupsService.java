package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroupType;
import com.example.forjavaschool.catalogmanagement.dto.OptionGroupDTO;
import com.example.forjavaschool.catalogmanagement.dto.OptionListDTO;
import com.example.forjavaschool.catalogmanagement.repositories.OptionGroupRepo;
import com.example.forjavaschool.catalogmanagement.repositories.OptionsRepo;
import com.example.forjavaschool.catalogmanagement.viewmodels.CompatibleOptionGroupView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionGroupView;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OptionGroupsService {
    @Autowired
    OptionGroupRepo optionGroupRepo;

    @Autowired
    OptionsRepo optionsRepo;

    @Autowired
    OptionsService optionsService;

    public List<OptionGroup> findAllOptionGroups() {
        return optionGroupRepo.findAll();
    }

    public List<OptionGroupView> findAllOptionGroupViews() {
        return findAllOptionGroups().stream()
                .map(OptionGroupView::new)
                .sorted()
                .collect(Collectors.toList());
    }

    public OptionGroup getOptionGroupById(int id) {
        return optionGroupRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundInDBException(id, "Option group"));
    }

    private EntityCannotBeSavedException instantiateEntityCannotBeSavedException(DataIntegrityViolationException e) {
        String errorMessage;
        if (e.getCause().getCause().toString().contains("duplicate key")) {
            if (e.getCause().getCause().toString().contains("Key (name)")) {
                errorMessage = "Option group with the same name already exists";
            } else {
                errorMessage = "Duplicate key value violates unique constraint";
            }
        } else {
            errorMessage = e.getMessage();
        }
        return new EntityCannotBeSavedException("Option group", errorMessage);
    }


    public OptionGroup saveNewGroup(OptionGroupDTO groupDTO) {
        OptionGroup group = getGroupFieldsFromDTO(groupDTO);
        getGroupWithUpdatedOptions(group, groupDTO);
        try {
            return optionGroupRepo.save(group);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }

    }

    private OptionGroup getGroupFieldsFromDTO(OptionGroupDTO dto) {
        OptionGroup group = new OptionGroup();
        group.setName(dto.getGroupName());
        group.setDescription(dto.getDescription());
        group.setIconURI(dto.getIconURI());
        group.setGroupType(OptionGroupType.getByIndex(dto.getGroupTypeIndex()));
        return group;
    }


    public OptionGroup saveEveryOptionInGroup(OptionGroup group) {
        Set<Option> currentOptions = group.getOptions();
        //convert to array to avoid concurrent modification exception:
        Option[] optionArray = currentOptions.toArray(new Option[0]);
        for (int i = 0; i < optionArray.length; i++) {
            optionArray[i].setGroup(group);
            optionsRepo.save(optionArray[i]);
        }
        return group;
    }

    private OptionGroup removeOption(OptionGroup optionGroup, Option option) {
        optionGroup.getOptions().remove(option);
        option.setGroup(null);
        optionsRepo.save(option);
        return optionGroup;
    }

    public OptionGroup updateOptionGroupFields(int id, OptionGroupDTO groupDTO) {
        OptionGroup optionGroup = optionGroupRepo.getById(id);
        optionGroup.setName(groupDTO.getGroupName());
        optionGroup.setDescription(groupDTO.getDescription());
        optionGroup.setIconURI(groupDTO.getIconURI());
        optionGroup.setGroupType(OptionGroupType.getByIndex(groupDTO.getGroupTypeIndex()));

        try {
            return optionGroupRepo.save(optionGroup);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    private OptionGroup deleteOptionsFromGroup(OptionGroup group) {
        Set<Option> options = group.getOptions();
        //convert to array to avoid concurrent modification exception:
        Option[] optionArray = options.toArray(new Option[0]);
        for (int i = 0; i < optionArray.length; i++) {
            removeOption(group, optionArray[i]);
            optionsRepo.save(optionArray[i]);
        }

        //options.forEach(option -> removeOption(group, option));
        try {
            return optionGroupRepo.save(group);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }


    private void setOptionListInGroup(OptionGroup group, Set<Integer> newOptionIDs) {
        if (newOptionIDs != null) {
            Set<Option> oldOptions = group.getOptions();
            Set<Option> newOptionsSet = optionsService.getOptionsSetByIDs(newOptionIDs);

            //go through new options and update those which do not exist yet in the old set of options:
            for (Option newOption : newOptionsSet) {
                if (!oldOptions.contains(newOption)) {
                    oldOptions.add(newOption);
                    newOption.setGroup(group);
                    optionsRepo.save(newOption);
                }
            }

            //go through old options and update those which do not exist new set of options:
            //array to avoid concurrent modification exception
            Option[] oldOptionsArray = oldOptions.toArray(new Option[0]);

            for (int i = 0; i < oldOptionsArray.length; i++) {
                if (!newOptionsSet.contains(oldOptionsArray[i])) {
                    oldOptions.remove(oldOptionsArray[i]);
                    oldOptionsArray[i].setGroup(null);
                    optionsRepo.save(oldOptionsArray[i]);
                }
            }
            group.setOptions(oldOptions);
        } else {
            group.setOptions(Collections.emptySet());
        }
    }

    private OptionGroup getGroupWithUpdatedOptions(OptionGroup group, OptionGroupDTO dto) {
        Set<Integer> optionIDs = dto.getOptionIDs();
        setOptionListInGroup(group, optionIDs);
        return group;
    }

    public OptionGroup updateOptionsInGroup(OptionGroup group, OptionListDTO optionListDTO) {
        System.out.println("option group = " + group);
        setOptionListInGroup(group, optionListDTO.getOptionIDs());
        try {
            return optionGroupRepo.save(group);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    public Set<OptionPlainView> getGroupOptionViews(OptionGroup group) {
        Set<Option> options = group.getOptions();
        Set<OptionPlainView> optionViews = new HashSet<>();
        options.forEach(option -> {
            OptionPlainView view = new OptionPlainView(option);
            optionViews.add(view);
        });
        return optionViews;
    }

    public void deleteGroup(int id) {
        OptionGroup group = optionGroupRepo.getById(id);
        deleteOptionsFromGroup(group);
        optionGroupRepo.delete(group);
    }


    public SortedSet<CompatibleOptionGroupView> getGroupViewsWithSelectedOptionsSorted(Set<Option> selectedOptions) {
        SortedSet<CompatibleOptionGroupView> groupsSet = new TreeSet<>();
        Set<Integer> addedGroupIDs = new HashSet<>();

        for (Option option : selectedOptions) {
            OptionGroup foundGroup = option.getGroup();
            if (foundGroup != null) {
                if (!addedGroupIDs.contains(foundGroup.getId())) {
                    CompatibleOptionGroupView groupView = new CompatibleOptionGroupView(foundGroup, option);
                    groupsSet.add(groupView);
                    addedGroupIDs.add(foundGroup.getId());
                } else {
                    for (CompatibleOptionGroupView groupView : groupsSet) {
                        if (groupView.getGroupId() == foundGroup.getId()) {
                            groupView.addOptionToGroupView(option);
                        }
                    }
                }
            }
        }

        return groupsSet;
    }
}
