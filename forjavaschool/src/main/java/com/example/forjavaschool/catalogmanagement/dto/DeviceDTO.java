package com.example.forjavaschool.catalogmanagement.dto;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class DeviceDTO {
    @NotBlank(message = "{devicedto.brand.null.msg}")
    @Size(min = 2, max = 60, message = "{devicedto.brand.size.msg}")
    private String brand;
    @NotBlank(message = "{devicedto.model.null.msg}")
    @Size(min = 2, max = 60, message = "{devicedto.model.size.msg}")
    private String model;

    private String description;
    @NotNull(message = "{general.price.null.msg}")
    @Digits(integer = 5, fraction = 2, message = "{devicedto.price.size.msg}")
    @PositiveOrZero(message = "{general.price.negative.msg}")
    private BigDecimal soloPrice;
    private Integer imageID;

    public DeviceDTO() {
    }

    public DeviceDTO(String brand, String model, String description, BigDecimal soloPrice, Integer imageID) {
        this.brand = brand;
        this.model = model;
        this.description = description;
        this.soloPrice = soloPrice;
        this.imageID = imageID;
    }

    public DeviceDTO(Device device) {
        this.brand = device.getBrand();
        this.model = device.getModel();
        this.description = device.getDescription();
        this.soloPrice = device.getSoloPrice();
        if (device.getDeviceImage() != null) {
            this.imageID = device.getDeviceImage().getImageId();
        } else {
            this.imageID = null;
        }
    }


}
