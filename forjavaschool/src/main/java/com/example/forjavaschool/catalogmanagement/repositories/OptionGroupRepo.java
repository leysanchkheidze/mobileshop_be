package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionGroupRepo extends JpaRepository<OptionGroup, Integer> {

}
