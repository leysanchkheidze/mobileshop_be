package com.example.forjavaschool.catalogmanagement.dao;

import com.example.forjavaschool.catalogmanagement.dto.DeviceDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Entity
@Table(name = "DEVICES", uniqueConstraints={
        @UniqueConstraint(columnNames = {"brand", "model"})
})
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DEVICE_ID")
    private int deviceId;

    @Column
    @NotBlank
    @Size(min = 2, max = 60)
    private String brand;

    @Column
    @NotBlank
    @Size(min = 2, max = 60)
    private String model;


    /*@Column(name = "DEVICE_NAME", unique = true)
    @NotBlank
    @Size(min = 4, max = 60)
    private String name;*/

    @Column(name = "DEVICE_DESCRIPTION")
    private String description;

    @Column(name = "SOLO_PRICE")
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 4, fraction = 2)
    @NotNull
    private BigDecimal soloPrice;

    @Column(name = "CREATION_TIME")
    private LocalDateTime creationTime;

    @Column(name = "UPDATE_TIME")
    private LocalDateTime updateTime;

    @OneToOne
    @JoinColumn(name = "image_id", referencedColumnName = "IMAGE_ID", unique = true)
    private DeviceImage deviceImage;

    public Device() {
        this.creationTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
    }

    public Device(DeviceDTO deviceDTO) {
        this();
        this.brand = deviceDTO.getBrand();
        this.model = deviceDTO.getModel();
        this.description = deviceDTO.getDescription();
        this.soloPrice = deviceDTO.getSoloPrice();

    }

    public void setUpdateTime() {
        this.updateTime = LocalDateTime.now();
    }

    public static Device getNotFoundDevice(int id, String model) {
        Device notFoundDevice = new Device();
        notFoundDevice.setDeviceId(id);
        notFoundDevice.setModel(model);
        return notFoundDevice;
    }


}
