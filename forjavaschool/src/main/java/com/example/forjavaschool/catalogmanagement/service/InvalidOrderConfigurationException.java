package com.example.forjavaschool.catalogmanagement.service;

public class InvalidOrderConfigurationException extends RuntimeException {
    private String field;
    private String fieldError;

    public InvalidOrderConfigurationException() {
        super();
    }

    public InvalidOrderConfigurationException(String field, String fieldError) {
        super(String.format("%s is invalid: %s", field, fieldError));
        this.field = field;
        this.fieldError = fieldError;
    }

    public String getField() {
        return field;
    }

    public String getFieldError() {
        return fieldError;
    }
}
