package com.example.forjavaschool.catalogmanagement.viewmodels;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public class CompatibleTariffsView {
    DevicePlainView devicePlainView;
    List<TariffInOfferView> tariffInOfferViewList;
    boolean success;



    public static CompatibleTariffsView getNotFoundResult(int deviceId, Exception e) {
        return new CompatibleTariffsView(
                DevicePlainView.getNotFoundDeviceView(deviceId, e.getMessage()),
                new ArrayList<>(),
                false);
    }
}
