package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.catalogmanagement.dto.OptionListDTO;
import com.example.forjavaschool.catalogmanagement.dto.TariffDTO;
import com.example.forjavaschool.catalogmanagement.repositories.OffersRepo;
import com.example.forjavaschool.catalogmanagement.repositories.TariffsRepo;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TariffsService {
    @Autowired
    TariffsRepo tariffsRepo;

    @Autowired
    OffersRepo offersRepo;

    @Autowired
    OptionsService optionsService;


    public List<Tariff> findAllTariffs() {
        return tariffsRepo.findAll();
    }

    public List<TariffPlainView> findAllTariffsPlain() {
        return findAllTariffs().stream()
                .map(TariffPlainView::getTariffPlainView)
                .sorted()
                .collect(Collectors.toList());
    }

    private EntityCannotBeSavedException instantiateEntityCannotBeSavedException(DataIntegrityViolationException e) {
        String errorMessage;
        if (e.getCause().getCause().toString().contains("duplicate key")) {
            if (e.getCause().getCause().toString().contains("Key (tariff_name)")) {
                errorMessage = "Tariff with the same name already exists";
            } else {
                errorMessage = "Duplicate key value violates unique constraint";
            }
        } else {
            errorMessage = e.getMessage();
        }
        return new EntityCannotBeSavedException("Tariff", errorMessage);
    }

    public Tariff saveNewTariff(TariffDTO tariffDTO) {
        Tariff tariff = new Tariff();
        tariff.setTariffName(tariffDTO.getTariffName());
        tariff.setMonthlyPrice(tariffDTO.getMonthlyPrice());
        tariff.setTariffDescription(tariffDTO.getTariffDescription());
        tariff.setDataVolume(tariffDTO.getDataVolume());
        try {
            return tariffsRepo.save(tariff);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }

    }

    public Tariff getTariffById(int id) {
        return tariffsRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid tariff Id:" + id));
    }

    public void deleteTariffByID(int id) {
        //delete related offers
        List<Offer> relatedOffers = offersRepo.findByTariff(getTariffById(id));
        for (int i = 0; i < relatedOffers.size(); i++) {
            offersRepo.delete(relatedOffers.get(i));
        }

        //delete tariff itself
        tariffsRepo.deleteById(id);
    }

    public Tariff updateTariff(int id, TariffDTO newTariffDTO) {
        Tariff tariff = tariffsRepo.getById(id);

        tariff.setTariffName(newTariffDTO.getTariffName());
        tariff.setTariffDescription(newTariffDTO.getTariffDescription());
        tariff.setDataVolume(newTariffDTO.getDataVolume());
        tariff.setMonthlyPrice(newTariffDTO.getMonthlyPrice());
        tariff.setUpdateTime();

        try {
            return tariffsRepo.save(tariff);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    public Tariff updateCompatibleOptionsInTariff(int id, OptionListDTO optionListDTO) {
        Tariff tariff = tariffsRepo.getById(id);
        Set<Option> newOptions = optionsService.getOptionsSetByIDs(optionListDTO.getOptionIDs());
        tariff.setCompatibleOptions(newOptions);
        try {
            return tariffsRepo.save(tariff);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }


    public TariffPlainView getTariffPlainViewByID(int id) {
        try {
            Tariff tariff = tariffsRepo.getById(id);
            return TariffPlainView.getTariffPlainView(tariff);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundInDBException(id, "Tariff");
        }
    }


}
