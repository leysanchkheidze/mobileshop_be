package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Offer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class OfferView implements Comparable<OfferView> {
    private int offerId;
    private int tariffId;
    private String tariffName;
    private int deviceId;
    private String deviceBrand;
    private String deviceModel;
    private BigDecimal devicePrice;


    public OfferView(Offer offer) {
        this.offerId = offer.getOfferId();
        this.tariffId = offer.getTariff().getTariffId();
        this.tariffName = offer.getTariff().getTariffName();
        this.deviceId = offer.getDevice().getDeviceId();
        this.deviceBrand = offer.getDevice().getBrand();
        this.deviceModel = offer.getDevice().getModel();
        this.devicePrice = offer.getDevicePrice();
    }

    @Override
    public int compareTo(OfferView o) {
        return Integer.compare(this.offerId, o.getOfferId());
    }
}
