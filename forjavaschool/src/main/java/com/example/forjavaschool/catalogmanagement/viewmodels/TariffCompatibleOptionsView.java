package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class TariffCompatibleOptionsView {
    private int tariffId;
    private String tariffName;
    Set<Integer> compatibleOptionsIDs;
    Set<String> compatibleOptionsNames;


    public TariffCompatibleOptionsView() {
    }


    public TariffCompatibleOptionsView(Tariff tariff) {
        this.tariffId = tariff.getTariffId();
        this.tariffName = tariff.getTariffName();
        Set<Option> options = tariff.getCompatibleOptions();
        if (options == null || options.size() == 0) {
            this.compatibleOptionsIDs = Collections.emptySet();
            this.compatibleOptionsNames = Collections.emptySet();
        } else {
            this.compatibleOptionsIDs = tariff.getCompatibleOptions()
                    .stream()
                    .map(Option::getId)
                    .collect(Collectors.toSet());
            this.compatibleOptionsNames = tariff.getCompatibleOptions()
                    .stream()
                    .map(Option::getName)
                    .collect(Collectors.toSet());
        }
    }

    public TariffCompatibleOptionsView(TariffPlainView tariffPlainView) {
        this.tariffId = tariffPlainView.getTariffId();
        this.tariffName = tariffPlainView.getTariffName();
        if (tariffPlainView.getCompatibleOptionsIDs() == null || tariffPlainView.getCompatibleOptionsIDs().size() == 0) {
            this.compatibleOptionsIDs = Collections.emptySet();
        } else {
            this.compatibleOptionsIDs = tariffPlainView.getCompatibleOptionsIDs();
        }

        if (tariffPlainView.getCompatibleOptionsNames() == null || tariffPlainView.getCompatibleOptionsNames().size() == 0) {
            this.compatibleOptionsNames = Collections.emptySet();
        } else {
            this.compatibleOptionsNames = tariffPlainView.getCompatibleOptionsNames();
        }
    }


    public int getTariffId() {
        return tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public Set<Integer> getCompatibleOptionsIDs() {
        return compatibleOptionsIDs;
    }

    public Set<String> getCompatibleOptionsNames() {
        return compatibleOptionsNames;
    }
}
