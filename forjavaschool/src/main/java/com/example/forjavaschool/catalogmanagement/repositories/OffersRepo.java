package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OffersRepo extends JpaRepository<Offer, Integer> {
    List<Offer> findByTariff(Tariff tariff);
    List<Offer> findByDevice(Device device);

}
