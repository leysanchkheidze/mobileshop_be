package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
@ToString
public class TariffInOfferView implements Comparable<TariffInOfferView> {
    private Integer tariffId;
    private String tariffName;
    private BigDecimal monthlyPrice;
    private String tariffDescription;
    private Integer dataVolume;
    private int offerId;

    public TariffInOfferView(Tariff tariff, Offer offer) {
        this.tariffId = tariff.getTariffId();
        this.tariffName = tariff.getTariffName();
        this.monthlyPrice = tariff.getMonthlyPrice();
        this.tariffDescription = tariff.getTariffDescription();
        this.dataVolume = tariff.getDataVolume();
        this.offerId = offer.getOfferId();

    }

    public static TariffInOfferView getNotFoundTariff(int tariffId, String name) {
        return new TariffInOfferView(tariffId, name, null, "", 0, 0);
    }


    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TariffInOfferView))
            return false;
        TariffInOfferView tv = (TariffInOfferView) o;
        return tv.tariffId.equals(this.tariffId);
    }

    @Override
    public int hashCode() {
        int result = Integer.hashCode(tariffId);
        result = 31 * result + tariffName.hashCode();
        result = 31 * result + monthlyPrice.hashCode();
        result = 31 * result + tariffDescription.hashCode();
        result = 31 * result + Integer.hashCode(dataVolume);
        return result;
    }

    @Override
    public int compareTo(TariffInOfferView o) {
        return this.tariffName.compareTo(o.getTariffName());
    }
}
