package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import lombok.*;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Comparator;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OptionPlainView implements Comparable<OptionPlainView>, DisplayableOptionSelection {
    private int id;
    private String name;
    private String description;
    private URI iconURI;
    private BigDecimal price;
    private int groupId;
    private String groupName;
    private String groupDescription;
    private URI groupIconURI;

    public OptionPlainView(Option option) {
        this.setId(option.getId());
        this.setName(option.getName());
        this.setDescription(option.getDescription());
        this.setIconURI(option.getIconURI());
        this.setPrice(option.getPrice());

        OptionGroup group = option.getGroup();

        if (group != null) {
            this.setGroupId(group.getId());
            this.setGroupName(group.getName());
            this.setGroupDescription(group.getDescription());
            this.setGroupIconURI(group.getIconURI());
        } else {
            OptionGroupView emptyGroup = OptionGroupView.EMPTY_GROUP_VIEW;
            this.setGroupId(emptyGroup.getId());
            this.setGroupName(emptyGroup.getGroupName());
            this.setGroupDescription(emptyGroup.getDescription());
            this.setGroupIconURI(emptyGroup.getIconURI());
        }


    }


    @Override
    public int compareTo(OptionPlainView o) {
        if (this.groupId != -1 && o.getGroupId() != -1) {
            return Comparator.comparing(OptionPlainView::getGroupName)
                    .thenComparing(OptionPlainView::getName)
                    .compare(this, o);
        }
        return this.name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof OptionPlainView))
            return false;
        OptionPlainView view = (OptionPlainView) o;
        if (this.groupId != -1 && view.getGroupId() != -1) {
            return this.groupId == view.getGroupId()
                    && this.name.equals(view.getName())
                    && this.groupName.equals(view.getGroupName());
        }
        return this.name.equals(view.getGroupName());
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public OptionSelectionType getType() {
        if (groupId == 0 || groupId == -1) {
            return OptionSelectionType.OPTION_WITHOUT_GROUP;
        } else {
            return OptionSelectionType.OPTION_IN_GROUP;
        }
    }
}
