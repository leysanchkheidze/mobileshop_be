package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.catalogmanagement.dto.OfferDTO;
import com.example.forjavaschool.ordermanagement.viewmodels.OrderConfigurationView;
import com.example.forjavaschool.catalogmanagement.repositories.OffersRepo;
import com.example.forjavaschool.catalogmanagement.viewmodels.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.example.forjavaschool.ForjavaschoolApplication.globalLogger;

@Service
public class OffersService {


    @Autowired
    OffersRepo offersRepo;

    @Autowired
    DevicesService devicesService;

    @Autowired
    TariffsService tariffsService;

    @Autowired
    OptionsService optionsService;

    @Autowired
    OptionGroupsService optionGroupsService;


    public List<OfferView> findAllOfferViews() {
        return offersRepo.findAll()
                .stream()
                .map(OfferView::new)
                .sorted()
                .collect(Collectors.toList());
    }

    public Offer getOfferById(int id) {
        return offersRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundInDBException(id, "Offer"));
    }

    private EntityCannotBeSavedException instantiateEntityCannotBeSavedException(DataIntegrityViolationException e) {
        String errorMessage;
        if (e.getCause().getCause().toString().contains("duplicate key")) {
            if (e.getCause().getCause().toString().contains("Key (tariff_id, device_id)")) {
                errorMessage = "Offer with same combination of tariff and device already exists";
            } else {
                errorMessage = "Duplicate key value violates unique constraint";
            }
        } else {
            errorMessage = e.getMessage();
        }
        return new EntityCannotBeSavedException("Offer", errorMessage);
    }

    public Offer saveOffer(OfferDTO offerDTO) {
        Tariff tariff = tariffsService.getTariffById(offerDTO.getTariffId());
        Device device = devicesService.getDeviceById(offerDTO.getDeviceId());
        Offer offer = new Offer(tariff, device, offerDTO.getDevicePrice());
        try {
            return offersRepo.save(offer);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }

    }

    public void deleteOfferByID(int id) {
        offersRepo.deleteById(id);
    }


    public List<DeviceInOfferView> getDeviceInOfferViewListByTariffId(int tariffId) {
        Tariff tariff = tariffsService.getTariffById(tariffId);
        List<Offer> offers = offersRepo.findByTariff(tariff);
        return offers.stream()
                .map(this::getDeviceInOfferView)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public List<TariffInOfferView> getTariffInOfferViewListByDeviceId(int deviceId) {
        Device device = devicesService.getDeviceById(deviceId);
        List<Offer> offers = offersRepo.findByDevice(device);
        return offers.stream()
                .map(this::getTariffInOfferView)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }


    public CompatibleDevicesView getCompatibleDeviceView(int tariffId) {
        try {
            TariffPlainView tariffPlainView = tariffsService.getTariffPlainViewByID(tariffId);
            List<DeviceInOfferView> compatibleDevices = getDeviceInOfferViewListByTariffId(tariffId);
            return new CompatibleDevicesView(tariffPlainView, compatibleDevices, true);
        } catch (EntityNotFoundInDBException e) {
            globalLogger.warn(e.getMessage());
            return CompatibleDevicesView.getNotFoundResult(tariffId, e);
        }
    }

    public CompatibleTariffsView getCompatibleTariffView(int deviceId) {
        try {
            Device device = devicesService.getDeviceById(deviceId);
            DevicePlainView devicePlainView = devicesService.getDevicePlainView(device);
            List<TariffInOfferView> tariffInOfferViewList = getTariffInOfferViewListByDeviceId(deviceId);
            return new CompatibleTariffsView(devicePlainView, tariffInOfferViewList, true);
        } catch (EntityNotFoundInDBException e) {
            globalLogger.warn(e.getMessage());
            return CompatibleTariffsView.getNotFoundResult(deviceId, e);
        }
    }


    public Offer updateOffer(int id, OfferDTO newOfferDTO) {
        Offer offer = offersRepo.getById(id);

        Tariff tariff = tariffsService.getTariffById(newOfferDTO.getTariffId());
        Device device = devicesService.getDeviceById(newOfferDTO.getDeviceId());

        offer.setTariff(tariff);
        offer.setDevice(device);
        offer.setDevicePrice(newOfferDTO.getDevicePrice());
        try {
            return offersRepo.save(offer);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }


    public OrderConfigurationView getOrderConfiguration(int offerId) {
        Offer offer = getOfferById(offerId);
        TariffInOfferView tariffInOfferView = getTariffInOfferView(offer);
        DeviceInOfferView deviceInOfferView = getDeviceInOfferView(offer);
        Set<Option> compatibleOptions = offer.getTariff().getCompatibleOptions();

        Set<Integer> optionsWithoutGroupIDs = compatibleOptions.stream()
                .filter(option -> option.getGroup() == null)
                .map(Option::getId)
                .collect(Collectors.toSet());

        SortedSet<OptionPlainView> optionWithoutGroupPlainViewSet = new TreeSet<>();
        for (Option option : optionsService.getOptionsSetByIDs(optionsWithoutGroupIDs)) {
            optionWithoutGroupPlainViewSet.add(new OptionPlainView(option));
        }
        SortedSet<CompatibleOptionGroupView> optionsInGroupsViewSet =
                optionGroupsService.getGroupViewsWithSelectedOptionsSorted(compatibleOptions);
        return new OrderConfigurationView(offerId,
                tariffInOfferView,
                deviceInOfferView,
                optionsInGroupsViewSet,
                optionWithoutGroupPlainViewSet);
    }

    public TariffInOfferView getTariffInOfferView(Offer offer) {
        Tariff tariff = offer.getTariff();
        return new TariffInOfferView(tariff, offer);
    }

    public DeviceInOfferView getDeviceInOfferView(Offer offer) {
        Device device = devicesService.getDeviceById(offer.getDevice().getDeviceId());
        return new DeviceInOfferView(device, offer);
    }


}
