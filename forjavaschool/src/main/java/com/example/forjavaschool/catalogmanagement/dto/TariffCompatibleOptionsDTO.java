package com.example.forjavaschool.catalogmanagement.dto;

import java.util.Collections;
import java.util.Set;

public class TariffCompatibleOptionsDTO {
    private int tariffID;
    private Set<Integer> optionsIDs;

    public TariffCompatibleOptionsDTO() {
    }

    public TariffCompatibleOptionsDTO(int tariffID, Set<Integer> optionsIDs) {
        this.tariffID = tariffID;
        if (optionsIDs == null || optionsIDs.size() == 0) {
            this.optionsIDs = Collections.emptySet();
        } else {
            this.optionsIDs = optionsIDs;
        }
    }

    public int getTariffID() {
        return tariffID;
    }

    public Set<Integer> getOptionsIDs() {
        return optionsIDs;
    }

    public void setTariffID(int tariffID) {
        this.tariffID = tariffID;
    }

    public void setOptionsIDs(Set<Integer> optionsIDs) {
        if (optionsIDs == null || optionsIDs.size() == 0) {
            this.optionsIDs = Collections.emptySet();
        } else {
            this.optionsIDs = optionsIDs;
        }

    }
}
