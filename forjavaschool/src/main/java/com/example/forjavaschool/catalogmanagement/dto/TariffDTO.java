package com.example.forjavaschool.catalogmanagement.dto;

import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@ToString
@Setter
public class TariffDTO {

    @NotBlank(message = "{general.name.null.msg}")
    @Size(min = 4, max = 40, message = "{optiondto.name.size.msg}")
    private String tariffName;
    @NotNull(message = "{general.price.null.msg}")
    @Digits(integer = 4, fraction = 2, message = "{tariffdto.price.size.msg}")
    @PositiveOrZero(message = "{general.price.negative.msg}")
    private BigDecimal monthlyPrice;
    private String tariffDescription;
    @PositiveOrZero(message = "{tariffdto.datavolume.negative.msg}")
    private Integer dataVolume;


    public TariffDTO(String tariffName, BigDecimal monthlyPrice, String tariffDescription, Integer dataVolume) {
        this.tariffName = tariffName;
        this.monthlyPrice = monthlyPrice;
        this.tariffDescription = tariffDescription;
        this.dataVolume = dataVolume;
    }

    public TariffDTO() {
    }

    public TariffDTO(TariffPlainView tariffPlainView) {
        this.tariffName = tariffPlainView.getTariffName();
        this.monthlyPrice = tariffPlainView.getMonthlyPrice();
        this.tariffDescription = tariffPlainView.getTariffDescription();
        this.dataVolume = tariffPlainView.getDataVolume();
    }
}
