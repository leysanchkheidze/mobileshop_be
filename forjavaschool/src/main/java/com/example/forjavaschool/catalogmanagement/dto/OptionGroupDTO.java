package com.example.forjavaschool.catalogmanagement.dto;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@ToString
@Setter
public class OptionGroupDTO {

    @NotBlank(message = "{general.name.null.msg}")
    @Size(min = 4, max = 40, message = "{optiongroupdto.name.size.msg}")
    private String groupName;
    private String description;
    private URI iconURI;
    private Set<Integer> optionIDs;
    private Integer groupTypeIndex;

    public OptionGroupDTO(){}

    public OptionGroupDTO(String groupName, String description, URI iconURI, Set<Integer> optionIDs, Integer groupTypeIndex) {
        this.groupName = groupName;
        this.description = description;
        this.iconURI = iconURI;
        this.optionIDs = optionIDs;
        this.groupTypeIndex = groupTypeIndex;
    }

    public OptionGroupDTO(OptionGroup optionGroup) {
        this.groupName = optionGroup.getName();
        this.description = optionGroup.getDescription();
        this.iconURI = optionGroup.getIconURI();
        this.optionIDs = optionGroup.getOptions()
                .stream()
                .map(Option::getId)
                .collect(Collectors.toSet());
        this.groupTypeIndex = optionGroup.getGroupType().typeIndex;
    }


}
