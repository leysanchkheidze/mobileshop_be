package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroupType;

import java.net.URI;
import java.util.SortedSet;
import java.util.TreeSet;

public class CompatibleOptionGroupView implements Comparable<CompatibleOptionGroupView> {
    private int groupId;
    private String groupName;
    private String groupDescription;
    private URI groupIconURI;
    private SortedSet<OptionPlainView> options = new TreeSet<>();
    private OptionGroupType groupType;

    public CompatibleOptionGroupView() {
    }

    public CompatibleOptionGroupView(OptionGroup group, Option option) {
        this.groupId = group.getId();
        this.groupName = group.getName();
        this.groupDescription = group.getDescription();
        this.groupIconURI = group.getIconURI();
        this.groupType = group.getGroupType();
        addOptionToGroupView(option);
    }

    public void addOptionToGroupView(Option newOption) {
        OptionPlainView optionView = new OptionPlainView(newOption);
        options.add(optionView);
    }


    public int getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public URI getGroupIconURI() {
        return groupIconURI;
    }

    public SortedSet<OptionPlainView> getOptions() {
        return options;
    }

    public OptionGroupType getGroupType() {
        return groupType;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public void setGroupIconURI(URI groupIconURI) {
        this.groupIconURI = groupIconURI;
    }

    public void setOptions(SortedSet<OptionPlainView> options) {
        this.options = options;
    }

    public void setGroupType(OptionGroupType groupType) {
        this.groupType = groupType;
    }

    @Override
    public int compareTo(CompatibleOptionGroupView o) {
        return this.groupName.compareTo(o.getGroupName());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof CompatibleOptionGroupView))
            return false;
        CompatibleOptionGroupView view = (CompatibleOptionGroupView) o;
        return this.groupName.equals(view.getGroupName());
    }
}
