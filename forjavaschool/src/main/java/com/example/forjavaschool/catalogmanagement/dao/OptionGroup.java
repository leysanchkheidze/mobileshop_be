package com.example.forjavaschool.catalogmanagement.dao;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.net.URI;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "OPTION_GROUPS")
public class OptionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "OPTION_GROUP_ID")
    private int id;
    @Column(unique = true)
    @NotBlank
    @Size(min = 4, max = 40)
    private String name;
    @Column
    private String description;
    @Column(name = "ICON_URI")
    private URI iconURI;
    @Column
    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Option> options;
    @Column
    OptionGroupType groupType;


    /*public void addOption(Option option) {
        options.add(option);
        option.setGroup(this);
    }

    public void removeOption(Option option) {

    }*/

   @Override
    public String toString() {
        return "Option group [" + name + "]";
    }

}
