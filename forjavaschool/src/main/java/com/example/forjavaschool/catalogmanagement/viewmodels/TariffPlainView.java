package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Getter
@ToString
public class TariffPlainView implements Comparable<TariffPlainView> {

    private final Integer tariffId;
    private final String tariffName;
    private final BigDecimal monthlyPrice;
    private final String tariffDescription;
    private final Integer dataVolume;
    private final Set<Integer> compatibleOptionsIDs;
    private final SortedSet<String> compatibleOptionsNames;

    public TariffPlainView(Integer tariffId, String tariffName, BigDecimal monthlyPrice, String tariffDescription, Integer dataVolume, Set<Integer> compatibleOptionsIDs, Set<String> compatibleOptionsNames) {
        this.tariffId = tariffId;
        this.tariffName = tariffName;
        this.monthlyPrice = monthlyPrice;
        this.tariffDescription = tariffDescription;
        this.dataVolume = dataVolume;
        if (compatibleOptionsIDs == null || compatibleOptionsIDs.size() == 0) {
            this.compatibleOptionsIDs = Collections.emptySet();
        } else {
            this.compatibleOptionsIDs = compatibleOptionsIDs;
        }

        if (compatibleOptionsNames == null || compatibleOptionsIDs.size() == 0) {
            this.compatibleOptionsNames = new TreeSet<>();
        } else {
            this.compatibleOptionsNames = new TreeSet<>(compatibleOptionsNames);
                  //  compatibleOptionsNames;
        }
    }


    public static TariffPlainView getTariffPlainView(Tariff tariff) {
        Set<Option> options = tariff.getCompatibleOptions();
        Set<Integer> optionIDs = options.stream()
                .map(Option::getId)
                .collect(Collectors.toSet());

        Supplier<SortedSet<String>> opts = TreeSet::new;
        SortedSet<String> optionNames = options.stream()
                .map(Option::getName)
                .sorted()
                .collect(Collectors.toCollection(opts));

        return new TariffPlainView(
                tariff.getTariffId(),
                tariff.getTariffName(),
                tariff.getMonthlyPrice(),
                tariff.getTariffDescription(),
                tariff.getDataVolume(),
                optionIDs,
                optionNames);
    }

    public static TariffPlainView getNotFoundTariff(int tariffId, String name) {
        return new TariffPlainView(tariffId, name, null, "", 0, Collections.emptySet(), Collections.emptySet());
    }

    @Override
    public int compareTo(TariffPlainView o) {
        return this.tariffName.compareTo(o.getTariffName());
    }
}
