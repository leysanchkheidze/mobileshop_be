package com.example.forjavaschool.catalogmanagement.dao;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "TARIFFS")
public class Tariff {
    private static final long serialVersionUID = 7L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TARIFF_ID")
    private int tariffId;

    @Column(name = "TARIFF_NAME", unique = true)
    @NotBlank
    @Size(min = 4, max = 40)
    private String tariffName;

    @Column(name = "MONTHLY_PRICE")
    @DecimalMin(value = "0.0", inclusive = false)
    @DecimalMax(value = "999.99")
    @Digits(integer = 3, fraction = 2)
    @NotNull
    private BigDecimal monthlyPrice;

    @Column(name = "CREATION_TIME")
    private final LocalDateTime creationTime;

    @Column(name = "UPDATE_TIME")
    private LocalDateTime updateTime;

    @Column(name = "TARIFF_UUID", unique = true)
    @NaturalId(mutable = true)
    private UUID contactUUID;

    @Column(name = "DESCRIPTION")
    private String tariffDescription;

    @Column(name = "DATA_VOLUME")
    private Integer dataVolume;

    @ManyToMany
    @JoinTable(name = "tariffs_options")
    private Set<Option> compatibleOptions;


    public Tariff() {
        this.creationTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
        this.contactUUID = UUID.randomUUID();
    }

    public Tariff(String tariffName, BigDecimal monthlyPrice, String tariffDescription, int dataVolume) {
        this.tariffName = tariffName;
        this.monthlyPrice = monthlyPrice;
        this.tariffDescription = tariffDescription;
        this.dataVolume = dataVolume;
        this.creationTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
        this.contactUUID = UUID.randomUUID();
    }

    public void setUpdateTime() {
        this.updateTime = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return String.format("Tariff [name: %s, id: %d, price: %s, option id's: %s",
                getTariffName(),
                getTariffId(),
                getMonthlyPrice(),
                getCompatibleOptions().stream().map(Option::getId).collect(Collectors.toSet()));
    }

}
