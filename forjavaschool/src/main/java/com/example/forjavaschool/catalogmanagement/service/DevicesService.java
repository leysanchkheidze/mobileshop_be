package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.DeviceImage;
import com.example.forjavaschool.catalogmanagement.dto.DeviceDTO;
import com.example.forjavaschool.catalogmanagement.repositories.DeviceImageRepo;
import com.example.forjavaschool.catalogmanagement.repositories.DevicesRepo;
import com.example.forjavaschool.catalogmanagement.viewmodels.DevicePlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service
public class DevicesService {
    @Autowired
    DevicesRepo devicesRepo;

    @Autowired
    DeviceImageRepo deviceImageRepo;


    private List<Device> findAllDevices() {
        return devicesRepo.findAll();
    }

    public List<DevicePlainView> findAllDevicesPlain() {
        List<DevicePlainView> views = new ArrayList<>();
        for (Device device : findAllDevices()) {
            DevicePlainView view = new DevicePlainView(device);
            views.add(view);
        }
        Collections.sort(views);
        return views;
    }

    public Device getDeviceById(int id) {
        return devicesRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundInDBException(id, "Device"));
    }

    public DevicePlainView getDeviceViewByID(int id) {
        Device device = getDeviceById(id);
        return new DevicePlainView(device);
    }

    public DevicePlainView getDevicePlainView(Device device) {
        return new DevicePlainView(device);
    }

    private EntityCannotBeSavedException instantiateEntityCannotBeSavedException(DataIntegrityViolationException e) {
        String errorMessage;
        if (e.getCause().getCause().toString().contains("duplicate key")) {
            if (e.getCause().getCause().toString().contains("Key (device_name)")) {
                errorMessage = "Device with the same name already exists";
            } else if (e.getCause().getCause().toString().contains("Key (image_id)")) {
                errorMessage = "The image is already selected for another device";
            } else if (e.getCause().getCause().toString().contains("Key (brand, model)")) {
                errorMessage = "Device with the same brand and model already exists";
            } else {
                errorMessage = "Duplicate key value violates unique constraint";
            }
        } else {
            errorMessage = e.getMessage();
        }
        return new EntityCannotBeSavedException("Device", errorMessage);
    }

    public Device saveNewDevice(DeviceDTO deviceDTO) {
        Device device = new Device(deviceDTO);

        if (deviceDTO.getImageID() != null) {
            DeviceImage image = deviceImageRepo.getById(deviceDTO.getImageID());
            device.setDeviceImage(image);
        }
        try {
            return devicesRepo.save(device);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }

    }

    public void deleteDeviceByID(int id) {
        devicesRepo.deleteById(id);
    }

    public Device updateDevice(int id, DeviceDTO newDeviceDTO) {
        Device device = devicesRepo.getById(id);
        device.setBrand(newDeviceDTO.getBrand());
        device.setModel(newDeviceDTO.getModel());
        device.setDescription(newDeviceDTO.getDescription());
        if (newDeviceDTO.getImageID() != null) {
            DeviceImage image = deviceImageRepo.getById(newDeviceDTO.getImageID());
            device.setDeviceImage(image);
        } else {
            device.setDeviceImage(null);
        }
        device.setSoloPrice(newDeviceDTO.getSoloPrice());
        device.setUpdateTime();
        try {
            return devicesRepo.save(device);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    public Device removeImageLink(int id) {
        Device device = devicesRepo.getById(id);
        device.setDeviceImage(null);
        device.setUpdateTime();
        return devicesRepo.save(device);


    }
}
