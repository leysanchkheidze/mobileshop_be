package com.example.forjavaschool.catalogmanagement.dao;

import javax.persistence.*;


@Entity
@Table(name = "DEVICE_IMAGES")
public class DeviceImage implements Comparable<DeviceImage>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IMAGE_ID")
    private int imageId;

    @Column
    private String fileName;
    @Column(unique = true)
    private String imageURL;

    @OneToOne(mappedBy = "deviceImage")
    private Device device;


    public DeviceImage() {
    }

    public int getImageId() {
        return imageId;
    }

    public String getFileName() {
        return fileName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public int compareTo(DeviceImage o) {
        return this.fileName.compareTo(o.getFileName());
    }
}
