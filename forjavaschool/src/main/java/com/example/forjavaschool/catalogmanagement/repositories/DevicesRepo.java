package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import org.springframework.data.jpa.repository.JpaRepository;

//TODO: в конце посмотреть, не заменить ли jpa на crudrepository
public interface DevicesRepo extends JpaRepository<Device, Integer> {

    boolean existsByModel(String model);

}
