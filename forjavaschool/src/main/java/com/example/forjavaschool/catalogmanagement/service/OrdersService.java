package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Device;
import com.example.forjavaschool.catalogmanagement.dao.Offer;
import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.ordermanagement.dto.OrderConfigurationItemsDTO;
import com.example.forjavaschool.ordermanagement.dao.Order;
import com.example.forjavaschool.ordermanagement.dao.OrderConfigurationItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OrdersService {

    @Autowired
    OffersService offersService;
    @Autowired
    TariffsService tariffsService;
    @Autowired
    DevicesService devicesService;
    @Autowired
    OptionsService optionsService;


    private Offer getValidatedOffer(int offerID) {
        try {
            return offersService.getOfferById(offerID);
        } catch (EntityNotFoundInDBException e) {
            throw new InvalidOrderConfigurationException("offerID",
                    e.getMessage());
        }
    }

    private Device getValidatedDevice(int deviceID, Offer offer) {
        try {
            if (offer.getDevice().getDeviceId() != deviceID) {
                throw new InvalidOrderConfigurationException("deviceID",
                        String.format("DeviceID %d doesn't match device id in the offer %d", deviceID, offer.getDevice().getDeviceId()));
            }
            return devicesService.getDeviceById(deviceID);
        } catch (EntityNotFoundInDBException e) {
            throw new InvalidOrderConfigurationException("deviceID",
                    e.getMessage());
        }
    }

    private Tariff getValidatedTariff(int tariffID, Offer offer) {
        try {
            if (offer.getTariff().getTariffId() != tariffID) {
                throw new InvalidOrderConfigurationException("deviceID",
                        String.format("TariffID %d doesn't match tariff id in the offer %d", tariffID, offer.getTariff().getTariffId()));
            }
            return tariffsService.getTariffById(tariffID);
        } catch (EntityNotFoundInDBException e) {
            throw new InvalidOrderConfigurationException("tariffID",
                    e.getMessage());
        }
    }

    private Set<Option> getValidatedOptionSet(Set<Integer> optionIDs, Tariff tariff) {
        try {
            Set<Option> optionSet = optionsService.getOptionsSetByIDs(optionIDs);
            for (Option option : optionSet) {
                if (!tariff.getCompatibleOptions().contains(option)) {
                    throw new InvalidOrderConfigurationException("opionID",
                            String.format("Option with id %d is not compatible with tariff %s", option.getId(), tariff.getTariffName()));
                }
            }
            return optionSet;
        } catch (EntityNotFoundInDBException e) {
            throw new InvalidOrderConfigurationException(e.getEntityName(), e.getMessage());
        }
    }

    public OrderConfigurationItems getOrderConfigItemsFromDTO(OrderConfigurationItemsDTO dto) {
        OrderConfigurationItems configurationItems = new OrderConfigurationItems();
        Offer offer = getValidatedOffer(dto.getOfferID());
        configurationItems.setOffer(offer);

        Device device = getValidatedDevice(dto.getDeviceID(), offer);
        configurationItems.setDevice(device);

        Tariff tariff = getValidatedTariff(dto.getTariffID(), offer);
        configurationItems.setTariff(tariff);

        Set<Option> options = getValidatedOptionSet(dto.getOptionIDs(), tariff);
        configurationItems.setOptions(options);
        return configurationItems;
    }

    public Order getOrderFromOrderConfigItems(OrderConfigurationItems orderConfigurationItems) {
        Order order = new Order();
        order.setOrderConfigurationItems(orderConfigurationItems);
        return order;
    }


}
