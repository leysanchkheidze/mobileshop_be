package com.example.forjavaschool.catalogmanagement.dto;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.net.URI;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class OptionDTO {
    @NotBlank(message = "{general.name.null.msg}")
    @Size(min = 4, max = 40, message = "{optiondto.name.size.msg}")
    private String name;
    private String description;
    private URI iconURI;
    @NotNull(message = "{general.price.null.msg}")
    @Digits(integer = 4, fraction = 2, message = "{optiondto.price.size.msg}")
    @PositiveOrZero(message = "{general.price.negative.msg}")
    private BigDecimal price;
    private Integer groupID;

    public OptionDTO() {
    }

    public OptionDTO(Option option) {
        this.name = option.getName();
        this.description = option.getDescription();
        this.iconURI = option.getIconURI();
        this.price = option.getPrice();
        if (option.getGroup() != null) {
            this.groupID = option.getGroup().getId();
        } else {
            this.groupID = -1;
        }
    }
}
