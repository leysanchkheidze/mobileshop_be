package com.example.forjavaschool.catalogmanagement.viewmodels;

public interface DisplayableOptionSelection {
    public OptionSelectionType getType();

    enum OptionSelectionType {
        OPTION_WITHOUT_GROUP,
        OPTION_IN_GROUP,
        GROUP;
    }


}

