package com.example.forjavaschool.catalogmanagement.service;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;
import com.example.forjavaschool.catalogmanagement.dto.OptionDTO;
import com.example.forjavaschool.catalogmanagement.repositories.OptionGroupRepo;
import com.example.forjavaschool.catalogmanagement.repositories.OptionsRepo;
import com.example.forjavaschool.catalogmanagement.repositories.TariffsRepo;
import com.example.forjavaschool.catalogmanagement.viewmodels.OptionPlainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.forjavaschool.ForjavaschoolApplication.globalLogger;

@Service
public class OptionsService {
    @Autowired
    OptionsRepo optionsRepo;

    @Autowired
    OptionGroupRepo optionGroupRepo;

    @Autowired
    TariffsRepo tariffsRepo;


    public List<OptionPlainView> findAllOptionViews() {
        return optionsRepo.findAll()
                .stream()
                .map(OptionPlainView::new)
                .sorted()
                .collect(Collectors.toList());
    }


    public Option getOptionById(int id) {
        return optionsRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundInDBException(id, "Option"));
    }

    public Option saveNewOption(OptionDTO optionDTO) {
        Option option = getOptionFromDTO(optionDTO);
        try {
            return optionsRepo.save(option);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    private EntityCannotBeSavedException instantiateEntityCannotBeSavedException(DataIntegrityViolationException e) {
        String errorMessage;
        if (e.getCause().getCause().toString().contains("duplicate key")) {
            if (e.getCause().getCause().toString().contains("Key (option_name)")) {
                errorMessage = "Option with the same name already exists";
            } else {
                errorMessage = "Duplicate key value violates unique constraint";
            }
        } else {
            errorMessage = e.getMessage();
        }
        return new EntityCannotBeSavedException("Option", errorMessage);
    }

    private Option getOptionFromDTO(OptionDTO optionDTO) {
        Option option = new Option();
        option.setName(optionDTO.getName());
        option.setDescription(optionDTO.getDescription());
        option.setIconURI(optionDTO.getIconURI());
        option.setPrice(optionDTO.getPrice());
        // groupId = -1 is passed from admin/options/new in case if no group was selected
        if (optionDTO.getGroupID() != null && optionDTO.getGroupID() != -1) {
            OptionGroup group = optionGroupRepo.getById(optionDTO.getGroupID());
            option.setGroup(group);
        }
        return option;
    }

    public Option updateOption(int id, OptionDTO optionDTO) {
        Option option = optionsRepo.getById(id);
        option.setName(optionDTO.getName());
        option.setDescription(optionDTO.getDescription());
        option.setIconURI(optionDTO.getIconURI());
        option.setPrice(optionDTO.getPrice());
        OptionGroup group = null;
        // groupId = -1 is passed from admin/options/new in case if no group was selected
        if (optionDTO.getGroupID() != null && optionDTO.getGroupID() != -1) {
            group = optionGroupRepo.getById(optionDTO.getGroupID());
        }
        option.setGroup(group);
        try {
            return optionsRepo.save(option);
        } catch (DataIntegrityViolationException e) {
            throw instantiateEntityCannotBeSavedException(e);
        }
    }

    public void deleteOption(int id) {
        try {
            Option option = optionsRepo.getById(id);
            //remove the option from group:
            option.setGroup(null);
            optionsRepo.save(option);
            //remove the option from every tariff which is compatible:
            removeOptionFromAllTariffs(option);
            //delete the option itself:
            optionsRepo.delete(option);
        } catch (EntityNotFoundInDBException e) {
            globalLogger.warn(e.getMessage());
        }
    }


    private void removeOptionFromAllTariffs(Option option) {
        List<Tariff> tariffsWithOption = tariffsRepo.findAll()
                .stream()
                .filter(tariff -> tariff.getCompatibleOptions().contains(option))
                .collect(Collectors.toList());
        for (int i = 0; i < tariffsWithOption.size(); i++) {
            Set<Option> tariffOptions = tariffsWithOption.get(i).getCompatibleOptions();
            tariffOptions.remove(option);
            tariffsWithOption.get(i).setCompatibleOptions(tariffOptions);
            tariffsRepo.save(tariffsWithOption.get(i));
        }
    }


    public Set<Option> getOptionsSetByIDs(Set<Integer> optionIDs) {
        Set<Option> optionSet = new HashSet<>();
        if (null != optionIDs && optionIDs.size() != 0) {
            for (int id : optionIDs) {
                Option option = getOptionById(id);
                optionSet.add(option);
            }
        }
        return optionSet;
    }
}
