package com.example.forjavaschool.catalogmanagement.dto;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.OptionGroup;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffCompatibleOptionsView;
import com.example.forjavaschool.catalogmanagement.viewmodels.TariffPlainView;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@ToString
@Setter
public class OptionListDTO {
    private Set<Integer> optionIDs;

    public OptionListDTO() {
    }

    public static OptionListDTO getOptionListDTO(OptionGroup optionGroup) {
        OptionListDTO listDTO = new OptionListDTO();
        listDTO.setOptionIDs(optionGroup.getOptions()
                .stream()
                .map(Option::getId)
                .collect(Collectors.toSet()));
        return listDTO;
    }

    public static OptionListDTO getOptionListDTO(TariffPlainView tariffPlainView) {
        OptionListDTO optionListDTO = new OptionListDTO();
        optionListDTO.setOptionIDs(tariffPlainView.getCompatibleOptionsIDs());
        return optionListDTO;
    }

    public static OptionListDTO getOptionListDTO(TariffCompatibleOptionsView tariffCompatibleOptionsView) {
        OptionListDTO optionListDTO = new OptionListDTO();
        optionListDTO.setOptionIDs(tariffCompatibleOptionsView.getCompatibleOptionsIDs());
        return optionListDTO;
    }
}
