package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.DeviceImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceImageRepo extends JpaRepository<DeviceImage, Integer> {
    public void deleteByFileName(String fileName);

    public DeviceImage findByFileName(String fileName);
}
