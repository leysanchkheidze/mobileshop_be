package com.example.forjavaschool.catalogmanagement.viewmodels;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import com.example.forjavaschool.catalogmanagement.dao.Tariff;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;


public class OptionCompatibleTariffsView {
    private int optionId;
    private String optionName;
    Set<Integer> compatibleTariffsIDs;

    public OptionCompatibleTariffsView() {
    }

    OptionCompatibleTariffsView(Option option) {
        this.optionId = option.getId();
        this.optionName = option.getName();
        Set<Tariff> compatibleTariffs = option.getCompatibleTariffs();
        if (compatibleTariffs == null || compatibleTariffs.size() == 0) {
            this.compatibleTariffsIDs = Collections.emptySet();
        } else {
            this.compatibleTariffsIDs = compatibleTariffs.stream()
                    .map(Tariff::getTariffId)
                    .collect(Collectors.toSet());
        }

    }

    public int getOptionId() {
        return optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public Set<Integer> getCompatibleTariffsIDs() {
        return compatibleTariffsIDs;
    }


}
