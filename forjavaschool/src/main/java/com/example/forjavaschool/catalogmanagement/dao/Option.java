package com.example.forjavaschool.catalogmanagement.dao;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.net.URI;
import java.util.Set;

@Getter
@Setter
@Entity
@AllArgsConstructor
@Table(name = "OPTIONS")
public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "OPTION_ID")
    private int id;
    @Column(name = "OPTION_NAME", unique = true)
    @NotBlank
    @Size(min = 4, max = 40)
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ICON_URI")
    private URI iconURI;
    @Column
    @DecimalMin(value = "0.0")
    @Digits(integer = 4, fraction = 2)
    private BigDecimal price;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    private OptionGroup group;
    @ManyToMany(mappedBy = "compatibleOptions")
    private Set<Tariff> compatibleTariffs;


    @Override
    public String toString() {
        return String.format("Option [id: %d, name: %s, price: %s]",
                id, name, price);
    }

    public Option() {
    }

}
