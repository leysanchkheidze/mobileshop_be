package com.example.forjavaschool.catalogmanagement.repositories;

import com.example.forjavaschool.catalogmanagement.dao.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionsRepo extends JpaRepository<Option, Integer> {
}
