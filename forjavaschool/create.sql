create sequence hibernate_sequence start 1 increment 1;
create table tariffs (id int4 not null, tariff_uuid uuid, creation_time timestamp, monthly_price numeric(19, 2), tariff_name varchar(255), update_time timestamp, primary key (id));
alter table if exists tariffs add constraint UK_j1b2vvnxt3ss54ieyqd9el4ca unique (tariff_uuid);
create sequence hibernate_sequence start 1 increment 1;
create table tariffs (id int4 not null, tariff_uuid uuid, creation_time timestamp, monthly_price numeric(19, 2), tariff_name varchar(255), update_time timestamp, primary key (id));
alter table if exists tariffs add constraint UK_j1b2vvnxt3ss54ieyqd9el4ca unique (tariff_uuid);
